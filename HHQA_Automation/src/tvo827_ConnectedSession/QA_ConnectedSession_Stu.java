package tvo827_ConnectedSession;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.GetElementAttribute;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import tvo616_LandingPageScenarios.QA_BaseClass;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.openqa.selenium.By.id;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Class for student user to create a Connected Session
 */

public class QA_ConnectedSession_Stu extends QA_BaseClass {


    @Test(dataProvider = "Student")
    public void Connected_Session(String s_username, String s_password, String t_username, String t_password, int id) throws InterruptedException, IOException {
        driver.get(testenvironment.url());

        GetElement("loginTopLink").click();

        GetElement("usernameInput").sendKeys(s_username);

        GetElement("passInput").sendKeys(s_password);

        GetElement("loginButton").click();
        Thread.sleep(2000);
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("studentPage"))));
        System.out.println("Student landing page");
        // Pick Tutors
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("chooseTutor")))).isDisplayed();
        
        GetElement("chooseTutor").click();

        /**
         * Searching for tutor on pick tutor list
         */
        
        List<WebElement> PickTutor = driver.findElements(By.xpath(properties.getProperty("tutorList")));
        System.out.println("Pick tutor page");

        for (WebElement tutor : PickTutor) {
            String test;
            boolean offline;
            tutor = driver.findElement(By.xpath("//*[contains(text()," + t_username + ")]/following::button[@data-user-id=\"" + id + "\"]"));
            test = tutor.getAttribute("style");
            offline = test.contains("cursor: not-allowed; pointer-events: none;");
            if (offline == false) {
                driver.findElement(By.xpath("//*[contains(text()," + t_username + ")]/following::button[@data-user-id=\"" + id + "\"]")).click();
            } else {
                while (offline == true) {
                    driver.navigate().refresh();
                    Thread.sleep(1000);
                    tutor = driver.findElement(By.xpath("//*[contains(text()," + t_username + ")]/following::button[@data-user-id=\"" + id + "\"]"));
                    test = tutor.getAttribute("style");
                    offline = test.contains("cursor: not-allowed; pointer-events: none;");
                }
                driver.findElement(By.xpath("//*[contains(text()," + t_username + ")]/following::button[@data-user-id=\"" + id + "\"]")).click();
            }
        }
        System.out.println("Tutor is choose");

        /**
         * Start of Connected Session
         */

        // Cancel Request modal
        
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("statusRequest")))).isDisplayed();
            GetElement("requestingStatusBar").isDisplayed();
            // img
            GetElement("requestingImg").isDisplayed();
            // text
            assertEquals(getText("requestionTxt"), "You are in line for Tutor");
            assertEquals(getText("requestingTutor"), t_username + ".");

            // Audio button
            GetElement("AudioMic").isDisplayed();
            GetElement("AudioMic").isEnabled();
            System.out.println("Audio Buton");
            // End Request button
            assertEquals(getText("EndRequestTxt"), "End Request");
            GetElement("EndRequestBtn").isDisplayed();
            GetElement("EndRequestBtn").isEnabled();
            System.out.println("Ending Request - Student");
         

        // Modal for audio

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("audioModalDiv")))).isDisplayed();
        assertEquals(getText("selectAudio"), "Select Audio");
        assertEquals(getText("browserAudio"), "Browser Based Audio");
        GetElement("browserAudio").isDisplayed();
        GetElement("browserAudio").isEnabled();
        // audio button 2
        assertEquals(getText("phoneAudio"), "Phone Audio");
        GetElement("phoneAudio").isDisplayed();
        GetElement("phoneAudio").isEnabled();

        // no audio button 3

        assertEquals(getText("noAudio"), "No Audio");
        GetElement("noAudio").isDisplayed();
        GetElement("noAudio").isEnabled();
        Thread.sleep(1000);

        // Click No Audio button 3
        GetElement("noAudio").click();
        Thread.sleep(3000);

        // Connected Status Bar

        //div for status bar
        GetElement("connectedStatusBar").isDisplayed();
        System.out.println("Connected Status Bar Displayed - Student");
        // img
        GetElement("connectedImg").isDisplayed();
        System.out.println("Connected Image displayed - Student");
        // text
        assertEquals(getText("connectedTxt"), "Connected to:");
        assertEquals(getText("connectedTutor"), t_username);

        // Audio button
        GetElement("mic").isDisplayed();
        GetElement("mic").isEnabled();

        // End Session button
        assertEquals(getText("endSessionTxt"), "End Session");
        GetElement("endSessionBtn").isDisplayed();
        GetElement("endSessionBtn").isEnabled();

        // Whiteboard notification
        //GetElement("wNotification").click();
        Thread.sleep(3000);
        
        // Drawing into whiteboard
        WebElement canvas = driver.findElement(By.cssSelector("#app > div.whiteboards > div:nth-child(1) > div.whiteboard.active > div > canvas.upper-canvas"));
        Thread.sleep(3000);
        Actions act = new Actions(driver);
        act.moveToElement(canvas);
        act.clickAndHold().perform();
        act.moveByOffset(10, 60);
        act.moveByOffset(100, 10);
        act.moveByOffset(10, -60);
        act.moveByOffset(100, -10);
        act.moveByOffset(10, 20);
        act.moveByOffset(10, 20);
        act.moveByOffset(10, 10);
        act.moveByOffset(10, 0);
        act.moveByOffset(10, 0);
        act.moveByOffset(10, -10);
        act.moveByOffset(1, 0);
        act.moveByOffset(1, 0);
        act.moveByOffset(1, 0);
        act.moveByOffset(1, 0);
        act.moveByOffset(1, 0);
        act.moveByOffset(1, 0);
        act.release();
        act.build().perform();

        System.out.println("Drawing");
        Thread.sleep(2000);

        /**
         * Assertion for Chat, including chat windows and all messages that are send
         * between student and tutor
         */

        //Chat in connected session
        GetElement("chat").isDisplayed();
        GetElement("chat").isEnabled();
        GetElement("chat").click();
        Thread.sleep(1000);

        //chat window
        GetElement("chatWindow").isDisplayed();
        GetElement("chatWindow").isEnabled();
        // chat close btn
        GetElement("closeChat").isEnabled();
        GetElement("closeChat").isEnabled();
        // Text sms
        assertEquals(getText("txtPanel1"), "Text Message"); // assertion
        // emoji btn
        GetElement("emojiBtn").isEnabled();
        GetElement("emojiBtn").isDisplayed();
        // sms input
        GetElement("msgInput").isEnabled();
        GetElement("msgInput").isDisplayed();
        // send btn
        GetElement("sendBtn").isEnabled();
        GetElement("sendBtn").isDisplayed();
        assertEquals(getText("sendBtn"), "Send");

        GetElement("msgInput").sendKeys("Hello " + t_username + "");
        Thread.sleep(1000);
        GetElement("sendBtn").click();
        Thread.sleep(1000);
        //First sms
        GetElement("inbox1Edu").isDisplayed();
        GetElement("inbox1EduImg").isDisplayed();
        assertEquals(getText("inbox1EduName"), s_username + " :");
        assertEquals(getText("inbox1EduTxt"), "Hello " + t_username);
        GetElement("closeChat").click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("chatNotification")))).isDisplayed();
        Thread.sleep(1000);
        GetElement("chat").click();

        //chat window
        GetElement("chatWindow").isDisplayed();
        GetElement("chatWindow").isEnabled();
        // chat close btn
        GetElement("closeChat").isEnabled();
        GetElement("closeChat").isEnabled();
        // Text sms
        assertEquals(getText("txtPanel1"), "Text Message");
        // emoji btn
        GetElement("emojiBtn").isEnabled();
        GetElement("emojiBtn").isDisplayed();
        // sms input
        GetElement("msgInput").isEnabled();
        GetElement("msgInput").isDisplayed();
        //assertEquals(getText("msgInput"), "Type message here...");
        // send btn
        GetElement("sendBtn").isEnabled();
        GetElement("sendBtn").isDisplayed();
        assertEquals(getText("sendBtn"), "Send");
        //First sms
        GetElement("inbox1Edu").isDisplayed();
        GetElement("inbox1EduImg").isDisplayed();
        assertEquals(getText("inbox1EduName"), s_username + " :");
        assertEquals(getText("inbox1EduTxt"), "Hello " + t_username);
        //Check the second sms
        GetElement("inbox2Edu").isDisplayed();
        GetElement("inbox2EduImg").isDisplayed();
        assertEquals(getText("inbox2EduName"), t_username + " :");
        assertEquals(getText("inbox2EduTxt"), "Hello " + s_username);
        Thread.sleep(2000);
        GetElement("msgInput").sendKeys("Test 1");
        Thread.sleep(1000);
        GetElement("sendBtn").click();
        Thread.sleep(1000);

        //First sms
        GetElement("inbox1Edu").isDisplayed();
        GetElement("inbox1EduImg").isDisplayed();
        assertEquals(getText("inbox1EduName"), s_username + " :");
        assertEquals(getText("inbox1EduTxt"), "Hello " + t_username);
        //Check the second sms
        GetElement("inbox2Edu").isDisplayed();
        GetElement("inbox2EduImg").isDisplayed();
        assertEquals(getText("inbox2EduName"), t_username + " :");
        assertEquals(getText("inbox2EduTxt"), "Hello " + s_username);
        //Check the third sms
        GetElement("inbox3Edu").isDisplayed();
        GetElement("inbox3EduImg").isDisplayed();
        assertEquals(getText("inbox3EduName"), s_username + " :");
        assertEquals(getText("inbox3EduTxt"), "Test 1");
        screenShot(driver, "ChatClose");
        GetElement("closeChat").click();
        Thread.sleep(3000);
        screenShot(driver, "chatwait");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("chatNotification")))).isDisplayed();
        System.out.println("Chat has passed");
        Thread.sleep(2000);

        /** Ending the session */

        //End Session button
        GetElement("endSessionBtn").click();

        // End session modal
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("endSession")))).isDisplayed();
        assertEquals(getText("endSessionModalTxt"), "End Session");
        assertEquals(getText("endSessionModalTutor"), "Are you sure you want to end your session with " + t_username + "?");

        assertEquals(getText("endSessionYes"), "Yes");
        GetElement("endSessionYes").isDisplayed();
        GetElement("endSessionYes").isEnabled();
        assertEquals(getText("ensSessionNo"), "No");
        GetElement("ensSessionNo").isDisplayed();
        GetElement("ensSessionNo").isEnabled();
        GetElement("endSessionYes").click();
        System.out.println("End session modal pass Student");

        // Save session modal
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("saveChat")))).isDisplayed();
        assertEquals(getText("saveChatTitle"), "Save Session");
        assertEquals(getText("saveChatForm"), "Please name your session and click save.");
        driver.findElement(By.name("title")).isDisplayed();
        driver.findElement(By.name("title")).isEnabled();
        driver.findElement(By.name("title")).sendKeys("Test session");
        assertEquals(getText("saveChatBtn"), "Save");
        GetElement("saveChatBtn").isDisplayed();
        GetElement("saveChatBtn").isEnabled();
        driver.findElement(id("saveChat")).click();
        System.out.println("Save session modal passs");
        Thread.sleep(2000);

        //Exit Survey modal
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("exitSurvey")))).isDisplayed();
        assertEquals(getText("exitSurveyModal"), "Exit Survey");
        assertEquals(getText("exitSurveyTxt1"), "How was your experience?");
        assertEquals(getText("exitSurveyTxt2"), "Tell us what you think in the exit survey.");
        assertEquals(getText("exitSurveyYes"), "Yes");
        GetElement("exitSurveyYes").isDisplayed();
        GetElement("exitSurveyYes").isEnabled();
        assertEquals(getText("exitSurveySkip"), "Skip");
        GetElement("exitSurveySkip").isDisplayed();
        GetElement("exitSurveySkip").isEnabled();
        GetElement("exitSurveyYes").click();
        System.out.println("Exit Survey modal pass");
        System.out.println("End Session for Student");

        /**
         * Filling Exit Survey with valid data
         */

        // Exit Survey
        GetElement("exitSurveyMainContent").isDisplayed();
        System.out.println("Exit Survey page");
        assertEquals(getText("exitSurveyTitle"), "Exit Survey");

        assertEquals(getText("exitSurveyH2"), "Welcome");
        assertEquals(getText("exitSurveyParagraph"), "Thank you for taking a few minutes of your time to participate in our Exit Survey.\nYour opinion matters to us as your candid feedback will be very helpful for us to provide you with a better and more satisfying learning experience in the future.");
        assertEquals(getText("exitSurveyQ1"), "1Did we help you with your math?");
        GetElement("exitSurveyQ1Yes").isDisplayed();
        GetElement("exitSurveyQ1Yes").isEnabled();
        GetElement("exitSurveyQ1No").isDisplayed();
        GetElement("exitSurveyQ1No").isEnabled();
        //GetElement("exitSurveyQ1Yes").click();
        assertEquals(getText("exitSurveyQ2"), "2How likely are you to recommend Mathify to your friends or classmates?");
        GetElement("exitSurveyQ2_1").isDisplayed();
        GetElement("exitSurveyQ2_1").isEnabled();
        GetElement("exitSurveyQ2_1Txt").isDisplayed();
        GetElement("exitSurveyQ2_1Txt").isEnabled();
        assertEquals(getText("exitSurveyQ2_1Txt"), "Poor");
        GetElement("exitSurveyQ2_2").isDisplayed();
        GetElement("exitSurveyQ2_2").isEnabled();
        GetElement("exitSurveyQ2_3").isDisplayed();
        GetElement("exitSurveyQ2_3").isEnabled();
        GetElement("exitSurveyQ2_4").isDisplayed();
        GetElement("exitSurveyQ2_4").isEnabled();
        GetElement("exitSurveyQ2_5").isDisplayed();
        GetElement("exitSurveyQ2_5").isEnabled();
        GetElement("exitSurveyQ2_6").isDisplayed();
        GetElement("exitSurveyQ2_6").isEnabled();
        GetElement("exitSurveyQ2_7").isDisplayed();
        GetElement("exitSurveyQ2_7").isEnabled();
        GetElement("exitSurveyQ2_8").isDisplayed();
        GetElement("exitSurveyQ2_8").isEnabled();
        GetElement("exitSurveyQ2_9").isDisplayed();
        GetElement("exitSurveyQ2_9").isEnabled();
        GetElement("exitSurveyQ2_10").isDisplayed();
        GetElement("exitSurveyQ2_10").isEnabled();
        GetElement("exitSurveyQ2_10Txt").isDisplayed();
        GetElement("exitSurveyQ2_10Txt").isEnabled();
        assertEquals(getText("exitSurveyQ2_10Txt"), "Excellent");
        //GetElement("exitSurveyQ2_9").click();

        assertEquals(getText("exitSurveyQ3"), "3Please give us your feedback:");
        GetElement("exitSurveyQ3Input").sendKeys("End Session");
        Thread.sleep(1000);

        // Submit button
        assertEquals(getText("exitSurveySubmitBtn"), "Submit");
        GetElement("exitSurveySubmitBtn").isDisplayed();
        GetElement("exitSurveySubmitBtn").isEnabled();
        //Cancel button
        assertEquals(getText("exitSurveyCancelBtn"), "Cancel");
        GetElement("exitSurveyCancelBtn").isDisplayed();
        GetElement("exitSurveyCancelBtn").isEnabled();
        GetElement("exitSurveyCancelBtn").click();
        //GetElement("exitSurveySubmitBtn").click();
        System.out.println("Exit Survey is completed");

        // Logout
        GetElement("menuLogout").click();
        Thread.sleep(1000);
        GetElement("menuLogoutBtn").click();
        System.out.println("Student is logged out");
        System.out.println("Connected Session is successfully completed for Student");

    }

    @DataProvider(name = "Student")
    public Object[][] getDataFromDataprovider() {
        return new Object[][]
                {
                        {"vangjush", "admin", "vangjushedu", "admin", 6}

                };
    }

}