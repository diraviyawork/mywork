package tvo827_ConnectedSession;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import tvo616_LandingPageScenarios.QA_BaseClass;

import static org.openqa.selenium.By.id;
import static org.testng.AssertJUnit.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

/**
 * Class for Tutor user to create a connected session
 */

public class QA_ConnectedSession_Edu extends QA_BaseClass{


    @Test(dataProvider = "Educator")
    public void Connected_Session(String s_username, String s_password, String t_username, String t_password,int id) throws InterruptedException, IOException {
        driver.get(testenvironment.url());

        GetElement("loginTopLink").click();

        GetElement("usernameInput").sendKeys(t_username);

        GetElement("passInput").sendKeys(t_password);

        GetElement("loginButton").click();
        System.out.println("Educator landing page");

        // Go to whiteboard
        GetElement("startSessionBtn").click();

        /** Aceppting the Queue
         * Entering to a connected session with a student
         */

        //Accept the Queue
        WebDriverWait wait = new WebDriverWait(driver, 40);
            
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("studentQueueSpam")))).isDisplayed();
            Thread.sleep(6000);
            GetElement("queueBtn").isDisplayed();
            GetElement("queueBtn").isEnabled();
            GetElement("queueBtn").click();
            GetElement("queueModal").isDisplayed();
            Thread.sleep(1000);

            assertEquals(getText("queueTitle"), "Queue");
            System.out.println("Queue");
            GetElement("queueExitBtn").isDisplayed();
            GetElement("queueExitBtn").isEnabled();
            assertEquals(getText("queueUsername"), s_username.substring(0,1).toUpperCase() + s_username.substring(1));
            GetElement("queueStatus").isDisplayed();
            assertEquals(getText("queueStatus"), "Waiting");

            System.out.println("Pass");
            Thread.sleep(2000);
            GetElement("activateQueue").isDisplayed();
            GetElement("activateQueue").isEnabled();
            GetElement("activateQueue").click();
            System.out.println("Accepted the Request");
            Thread.sleep(5000);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("audioModalDiv"))));
            assertEquals(getText("selectAudio"), "Select Audio");
            assertEquals(getText("browserAudio"), "Browser Based Audio");
            GetElement("browserAudio").isDisplayed();
            GetElement("browserAudio").isEnabled();
            System.out.println("Audio Modal displayed for Educator");
            //  audio button 2
            assertEquals(getText("phoneAudio"), "Phone Audio");
            GetElement("phoneAudio").isDisplayed();
            GetElement("phoneAudio").isEnabled();

            // No audio button 3
            assertEquals(getText("noAudio"), "No Audio");
            GetElement("noAudio").isDisplayed();
            GetElement("noAudio").isEnabled();
            Thread.sleep(1000);

            // Click No Audio button 2
            //driver.findElement(By.xpath("//*[@id=\"app\"]/div[5]/div/div[6]/div[2]/button[2]")).click();
            // Click No Audio button 3
            GetElement("noAudio").click();
            Thread.sleep(2000);
            // Connected Status Bar
            //div for status bar
            GetElement("connectedStatusBar").isDisplayed();
            System.out.println("Connected Status bar Educator");
            // img
            GetElement("connectedImg").isDisplayed();
            System.out.println("Connected Image Educator");
            // text
            assertEquals(getText("connectedTxt"),"Connected to");
            assertEquals(getText("connectedStu"), s_username);

            // Audio button
            GetElement("mic").isDisplayed();
            GetElement("mic").isEnabled();

            // View Queue button
            assertEquals(getText("viewQueue"), "View Queue");
            GetElement("viewQueue").isDisplayed();
            GetElement("viewQueue").isEnabled();
            System.out.println("View Queue Verification");
            // End Session button
            //assertEquals(getText("endSessionTxt"), "End Session");
            GetElement("endSessionBtn").isDisplayed();
            GetElement("endSessionBtn").isEnabled();
            System.out.println("pass");
            System.out.println("Start connected session");

            //Chat in connected session

            //chat button
            GetElement("chat").isDisplayed();
            GetElement("chat").isEnabled();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("chatNotification")))).isDisplayed();
            Thread.sleep(1000);
            GetElement("chat").click();
            /**
             * Assertion for Chat, including chat window and all sms that are send
             * between student and tutor
             */
            //chat window
            GetElement("chatWindow").isDisplayed();
            GetElement("chatWindow").isEnabled();
            // chat close btn
            GetElement("closeChat").isEnabled();
            GetElement("closeChat").isEnabled();
            // Text sms
            assertEquals(getText("txtPanel1"), "Text Message"); // assertion
            //assertEquals(getText("txtPanel2"), "Voice Chat"); // assertion
            // emoji btn
            GetElement("emojiBtn").isEnabled();
            GetElement("emojiBtn").isDisplayed();
            // sms input
            GetElement("msgInput").isEnabled();
            GetElement("msgInput").isDisplayed();
            //assertEquals(getText("msgInput"), "Type message here...");
            // send btn
            GetElement("sendBtn").isEnabled();
            GetElement("sendBtn").isDisplayed();
            assertEquals(getText("sendBtn"), "Send");
            //Check the first sms
            GetElement("inbox1Edu").isDisplayed();
            GetElement("inbox1EduImg").isDisplayed();
            assertEquals(getText("inbox1EduName"), s_username+" :");
            assertEquals(getText("inbox1EduTxt"), "Hello " +t_username);
            GetElement("msgInput").sendKeys("Hello "+s_username+"");
            Thread.sleep(1000);
            GetElement("sendBtn").click();
            //Check if the first sms is displayed
            GetElement("inbox1Edu").isDisplayed();
            GetElement("inbox1EduImg").isDisplayed();
            assertEquals(getText("inbox1EduName"), s_username+" :");
            assertEquals(getText("inbox1EduTxt"), "Hello " +t_username);
            // Check the second sms
            GetElement("inbox2Edu").isDisplayed();
            GetElement("inbox2EduImg").isDisplayed();
            assertEquals(getText("inbox2EduName"), t_username+" :");
            assertEquals(getText("inbox2EduTxt"), "Hello " +s_username);
            Thread.sleep(1000);
            GetElement("closeChat").click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("chatNotification")))).isDisplayed();
            Thread.sleep(1000);
            System.out.println("Chat passed");
            WebElement canvas1 = driver.findElement(By.cssSelector("#app > div.whiteboards > div:nth-child(1) > div.whiteboard.active > div > canvas.upper-canvas")) ;
            Thread.sleep(3000);
            Actions act = new Actions(driver);
            act.moveToElement(canvas1);
            act.clickAndHold().perform();
            act.moveByOffset(-10, -60);
            act.moveByOffset(-100, 10);
            act.moveByOffset(-10,-60);
            act.moveByOffset(-100,-10);

            act.moveByOffset(10, 20);
            act.moveByOffset(10, 20);
            act.moveByOffset(10,10);
            act.moveByOffset(10,0);
            act.moveByOffset(10,0);
            act.moveByOffset(10,-10);
            act.moveByOffset(1,0);
            act.moveByOffset(1,0);
            act.moveByOffset(1,0);
            act.moveByOffset(1,0);
            act.moveByOffset(1,0);
            act.moveByOffset(1,0);
            act.release();
            act.build().perform();
            //----------------------------------------------------------
            Thread.sleep(2000);
            GetElement("chat").click();

            //Chat Window
            GetElement("chatWindow").isDisplayed();
            GetElement("chatWindow").isEnabled();
            // chat close btn
            GetElement("closeChat").isEnabled();
            GetElement("closeChat").isEnabled();
            // Text sms
            assertEquals(getText("txtPanel1"), "Text Message"); // assertion
            //assertEquals(getText("txtPanel2"), "Voice Chat"); // assertion
            // emoji btn
            GetElement("emojiBtn").isEnabled();
            GetElement("emojiBtn").isDisplayed();
            // sms input
            GetElement("msgInput").isEnabled();
            GetElement("msgInput").isDisplayed();
            //assertEquals(getText("msgInput"), "Type message here...");
            // send btn
            GetElement("sendBtn").isEnabled();
            GetElement("sendBtn").isDisplayed();
            assertEquals(getText("sendBtn"), "Send");

            //Check the first sms
            GetElement("inbox1Edu").isDisplayed();
            GetElement("inbox1EduImg").isDisplayed();
            assertEquals(getText("inbox1EduName"), s_username+" :");
            assertEquals(getText("inbox1EduTxt"), "Hello " +t_username);
            //Check the second sms
            GetElement("inbox2Edu").isDisplayed();
            GetElement("inbox2EduImg").isDisplayed();
            assertEquals(getText("inbox2EduName"), t_username+" :");
            assertEquals(getText("inbox2EduTxt"), "Hello " +s_username);
            //Check the third sms
            GetElement("inbox3Edu").isDisplayed();
            GetElement("inbox3EduImg").isDisplayed();
            assertEquals(getText("inbox3EduName"), s_username+" :");
            assertEquals(getText("inbox3EduTxt"), "Test 1");

            //Checking again if all sms are displayed correctly
            Thread.sleep(1000);
            GetElement("msgInput").sendKeys("Test 2");
            GetElement("sendBtn").click();
            //Check the first sms
            GetElement("inbox1Edu").isDisplayed();
            GetElement("inbox1EduImg").isDisplayed();
            assertEquals(getText("inbox1EduName"), s_username+" :");
            assertEquals(getText("inbox1EduTxt"), "Hello " +t_username);
            //Check the second sms
            GetElement("inbox2Edu").isDisplayed();
            GetElement("inbox2EduImg").isDisplayed();
            assertEquals(getText("inbox2EduName"), t_username+" :");
            assertEquals(getText("inbox2EduTxt"), "Hello " +s_username);
            //Check the third sms
            GetElement("inbox3Edu").isDisplayed();
            GetElement("inbox3EduImg").isDisplayed();
            assertEquals(getText("inbox3EduName"), s_username+" :");
            assertEquals(getText("inbox3EduTxt"), "Test 1");
            //Check the last sms
            GetElement("inbox4Edu").isDisplayed();
            GetElement("inbox4EduImg").isDisplayed();
            assertEquals(getText("inbox4EduName"), t_username+" :");
            assertEquals(getText("inbox4EduTxt"), "Test 2");


        /** End of the session */

            //Close the connected session
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("endSessionModal")))).isDisplayed();
            assertEquals(getText("endSessionModalBtn"), "Ok");
            GetElement("endSessionModalBtn").isDisplayed();
            GetElement("endSessionModalBtn").isEnabled();
            GetElement("endSessionModalBtn").click();
            System.out.println("Session end modal");

            // Sort Question modal
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("exitSurvey")))).isDisplayed();
            //assertEquals(getText("sortQuestionModalTitle"), "Sort Question");
            assertEquals(getText("sortQuestionModalTxt1"), "How was your experience?");
            assertEquals(getText("sortQuestionModalTxt2"), "Tell us what you think in the exit survey.");
            GetElement("sortQuestionBtn").isDisplayed();
            GetElement("sortQuestionBtn").isEnabled();
            GetElement("sortQuestionBtn").click();
            System.out.println("Sort Question modal pass");
            System.out.println("End session for Educator");


        /** Fill Sort Question with valid data */

            // Sort Question
            GetElement("sortQuestionMainContent").isDisplayed();
            System.out.println("Sort Question page");
            //assertEquals(getText("sortQuestionTitle"), "Sort Question");
            assertEquals(getText("sortQuestionTxt1"),"Please sort this question. Once it is categorized, it will appear in yours and the student's My Stuff.");
            assertEquals(getText("sortQuestionTxt2"),"If flagged as inappropriate, the session will not appear in My Stuff");
            assertEquals(getText("sortQuestionTxt4"), "Student Name");
            assertEquals(getText("sortQuestionStdName"), s_username);

            // Fill with data
            assertEquals(getText("sortQuestionQ1"), "Please pick a title for this session");
            GetElement("sortQuestionQ1Data").isEnabled();
            GetElement("sortQuestionQ1Data").isDisplayed();
            GetElement("sortQuestionQ1Data").sendKeys("Connected Session");

            assertEquals(getText("sortQuestionQ2"), "Grade of Question");
            Select Grade = new Select(driver.findElement(id("grade")));
            Grade.selectByVisibleText("Grade 10");
            Thread.sleep(2000);

            assertEquals(getText("sortQuestionQ3"), "Course Level of Question");
            Select Course = new Select(driver.findElement(id("courseCode")));
            Course.selectByVisibleText("MPM2D");
            Thread.sleep(1000);

            assertEquals(getText("sortQuestionQ4"), "Strand of Question");
            Select Strand = new Select(driver.findElement(id("strand")));
            Strand.selectByVisibleText("Trigonometry");

            assertEquals(getText("sortQuestionComment"), "Comments");
            GetElement("sortQuestionCommentArea").isEnabled();
            GetElement("sortQuestionCommentArea").sendKeys("Add some comments here!");

            //Flag
            assertEquals(getText("flagReview"), "Flag for review");
            assertEquals(getText("flagInappropriate"), "Flag as inappropriate");

            //Button
            assertEquals(getText("sortQuestionSubmitBtn"), "Submit");
            GetElement("sortQuestionSubmitBtn").isDisplayed();
            GetElement("sortQuestionSubmitBtn").isEnabled();
            GetElement("sortQuestionSubmitBtn").click();
            System.out.println("Sort Question is successfully completed");


        // Logout
        
            GetElement("menuLogout").click();
            Thread.sleep(1000);
            GetElement("menuLogoutBtn").click();
            System.out.println("Educator is logged out");
            System.out.println("Successfully connected session for Educator");
    }
    @DataProvider(name = "Educator")
    public Object[][] getDataFromDataprovider(){
        return new Object[][]
                {
                        {"vangjush", "admin", "vangjushedu", "admin", 30}
                };
    }
}
