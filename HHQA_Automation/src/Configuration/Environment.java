package Configuration;


import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;

@Sources({"classpath:${env}.properties"})

public interface Environment extends Config {

	String url();
	String registerStudURL();
	String registerEduURL();
}