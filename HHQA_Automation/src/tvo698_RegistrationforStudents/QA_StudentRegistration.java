package tvo698_RegistrationforStudents;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import tvo616_LandingPageScenarios.QA_BaseClass;

public class QA_StudentRegistration extends QA_BaseClass {
	
	String[] str = new String[100]; 


	@Test(priority=1) /* Student Registration */
	public void StudentRegistrationTest() throws InterruptedException, IOException{

		driver.get(testenvironment.url()); 
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//section[@class='c-hero']//div[@class='container']"))));
		
		GetElement("tvoLogo").isDisplayed();
		System.out.println("pass");
		GetElement("studentTopLink").isDisplayed();
		GetElement("educatorTopLink").isDisplayed();
		GetElement("parentsTopLink").isDisplayed();
		GetElement("registerTopLink").isEnabled();
		assertEquals(getText("registerTopLink"), "Register");
		GetElement("loginTopLink").isEnabled();
		assertEquals(getText("loginTopLink"), "Login");
		GetElement("activeEnable").isDisplayed();
		
		ElementClick("registerTopLink");
		GetElement("registerToolTip").isDisplayed();
		assertTrue(VerifyImage("studentMenuIcon"));
		assertEquals(getText("studentTextToolTip"), "Student");
		assertTrue(VerifyImage("educatorMenuIcon"));
		assertEquals(getText("educatorTextToolTip"), "Educator");
		driver.get(testenvironment.url()+"register/student");
		
		System.out.println("Student Register page - Student Registration program");
		GetElement("headerMain").isDisplayed();
		assertTrue(VerifyImage("studentRegistrationImg"));
		GetElement("studentRegisterWrapper").isDisplayed();
		assertEquals(getText("studentRegistrationTextXpath"), "Student Registration");
		str[0] = FindElements("registrationMandatoryCommentLineXpath");
		CompareStrings(str[0], ReadProperty("registrationMandatoryCommentLineText"));
		assertEquals(getText("usernameTextXpath"), "Username");
		assertEquals(getText("usernameMinCharacTextXpath"), "Minimum of 5 characters");
		GetElement("usernameField").isDisplayed();
		GetElement("usernameField").isEnabled();
		assertEquals(getText("emailAddressTextXpath"), "Email Address");
		GetElement("emailField").isDisplayed();
		GetElement("emailField").isEnabled();
		assertEquals(getText("passwordTextXpath"), "Password");
		assertEquals(getText("passwordMinCharacTextXpath"), "Minimum of 5 characters");
		GetElement("passwordField").isDisplayed();
		GetElement("passwordField").isEnabled();
		GetElement("togglePasswordIcon").isDisplayed();
		assertEquals(getText("oenTextXpath"), "OEN");
		//assertTrue(VerifyImage("oenQuestionImg"));
		assertEquals(getText("oenExpansionTextXpath"), "Ontario Educational Number");
		GetElement("oenField").isDisplayed();
		GetElement("oenField").isEnabled();
		assertEquals(getText("gradeTextXpath"), "Grade");
		GetElement("selectGradeFieldXpath").isDisplayed();
		GetElement("selectGradeFieldXpath").isEnabled();
		str[1] = FindElements("selectGradeOptionXpath");
		System.out.println(str[1]);
		assertEquals(getText("selectGradeOptionXpath"), "Select grade");
		assertEquals(getText("schoolBoardTextXpath"), "School Board/Organization");
		GetElement("selectBoardFieldXpath").isDisplayed();
		GetElement("selectBoardFieldXpath").isEnabled();
		str[2] = FindElements("selectBoardOptionXpath");
		System.out.println(str[2]);
		assertEquals(getText("selectBoardOptionXpath"), "Select Board");
		
		Select select = new Select(GetElement("selectGradeFieldXpath"));
		select.selectByValue("3");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='hide-course']")));
		
		assertEquals(getText("levelCourseTextXpath"), "Level/Course Code");
		GetElement("levelCourseFieldXpath").isDisplayed();
		GetElement("levelCourseFieldXpath").isEnabled(); 
		str[2] = FindElements("levelCourseOptionXpath");
		
		assertEquals(getText("levelCourseOptionXpath"), "Select Level/Course Code");
		GetElement("agreementCheckBox").isDisplayed();
		GetElement("agreementCheckBox").isEnabled();
		assertEquals(getText("agreementTextXpath"), "I agree to the Terms of Service and Privacy Policy");
	
		str[3] = RedirectLink("targetXpath", "Terms of Service");
		assertEquals(str[3], properties.getProperty("footerLinkTarget"));
		
		str[4] = RedirectLink("hrefXpath", "Terms of Service");
		assertEquals(str[4], properties.getProperty("termsHref"));
		
		str[5] = RedirectLink("targetXpath", "Privacy Policy");
		assertEquals(str[5], properties.getProperty("footerLinkTarget"));
		
		str[6] = RedirectLink("hrefXpath", "Privacy Policy");
		assertEquals(str[6], properties.getProperty("privacyHref"));
		
		GetElement("newsCheckBox").isDisplayed();
		GetElement("newsCheckBox").isEnabled();
		
		assertEquals(getText("newsSubscriptionTextXpath"), "Subscribe to our newsletters");
		
		GetElement("submitButton").isDisplayed();
		GetElement("submitButton").isEnabled();
	
		GetElement("footerMain").isDisplayed();
      	GetElement("footerSub").isDisplayed();

      	System.out.println("Success - Student Registration program");
}
	
}
