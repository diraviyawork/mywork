package tvo698_RegistrationforStudents;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import tvo616_LandingPageScenarios.QA_BaseClass;
public class QA_StudentRegistrationValidUser extends QA_BaseClass {
		
		String[] str = new String[100]; 

		@DataProvider(name="Validtestdata")
		public Object[][] testData1() {
			return new Object[][] {
				{ "QATest12345", "qatest12345@mail-tester.com", "Test123!", "505050500" , "4"} ,
				{ "123QATest", "123qatest@brocku.ca", "Test123!", "505050500" , "1" } ,
				{ "QATest2", "qatest2@tvo.org", "Test123!", "505050500", "3"} ,
				{ "QATEST666", "QATEST666@cpco.on.ca", "Test123!", "505050500","2"} 
			} ;
		}
		
		
		@Test(priority=1, dataProvider="Validtestdata") /* Student Registration */
		public void StudentRegistrationValidUserTest(String Name, String Email, String Password, String OEN, String grade) throws InterruptedException, IOException {
		    driver.get(testenvironment.url()+"register/student");
			WebDriverWait wait = new WebDriverWait(driver, 10);
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties.getProperty("usernameField"))));
			GetElement("usernameField").clear();
			GetElement("usernameField").sendKeys(Name);
			
			GetElement("emailField").clear();
			GetElement("emailField").sendKeys(Email);
			
			GetElement("passwordField").clear();
			GetElement("passwordField").sendKeys(Password);
			
			GetElement("oenField").clear();
			GetElement("oenField").sendKeys(OEN);	
			Select gradeSelect = new Select(GetElement("selectGradeFieldXpath"));
			gradeSelect.selectByValue(grade);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='courseCode']")));
			WebElement ele = driver.findElement(By.xpath("//select[@id='courseCode']"));
			js.executeScript("arguments[0].scrollIntoView(true);", ele);
			Select courseSelect = new Select(ele);
			
			if(grade == "3") {
				
				Thread.sleep(2000);
				courseSelect.selectByIndex(1);
				
				
			} else if (grade == "4") {
			
				Thread.sleep(2000);
				courseSelect.selectByIndex(2);
				
				
			} 
			
				ElementClick("selectBoardFieldXpath");
				Select boardSelect = new Select(GetElement("selectBoardFieldXpath"));
				boardSelect.selectByIndex(46);
				
				
			
			if (!GetElement("agreementCheckBox").isSelected()) {
				
				GetElement("agreementCheckBox").click(); //Agreement Selected
				
			} 
			

			WebElement el = GetElement("submitButton");
			js.executeScript("arguments[0].scrollIntoView(true);", el);
			gradeSelect.selectByVisibleText("Select grade");
			
			 System.out.println("Success - Student Registration Valid User program");
	}

}
