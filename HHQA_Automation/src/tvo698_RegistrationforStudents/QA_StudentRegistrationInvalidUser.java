package tvo698_RegistrationforStudents;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import tvo616_LandingPageScenarios.QA_BaseClass;

public class QA_StudentRegistrationInvalidUser extends QA_BaseClass {

			
	@DataProvider(name="Invalidtestdata")
	public Object[][] testData1() {
		return new Object[][] {
			{ "QA", "qatest1@mail-tester.com", "Test123!", "505050500" , "4", "studentRegisterGenericErrMsg1", false, "SS1"} ,
			{ "diyastud", "diya@skooli.com", "Test123!", "505050500" , "1", "studentRegisterExistingAccErrMsg",true, "SS2"} ,
			{	"", "", "", "", "", "studentRegisterGenericErrMsg2",false , "SS3"},
			{ "QATe’st4", "qatest4@dsb1.ca", "", "505050500" , "2","studentRegisterGenericErrMsg3", false, "SS4"} ,
			{ "QATest2", "qatest2@brocku.ca", "", "", "4","studentRegisterGenericErrMsg4",false, "SS5"} ,
			{ "QATest'''7", "qatest7@mail-tester.com", "Tes", "505050500", "", "studentRegisterGenericErrMsg5",false, "SS6"} ,
			{ "QA---Test8", "", "Test123!", "5050", "3", "studentRegisterGenericErrMsg6",false, "SS7"}  
		} ;
	}
	
	
	@Test(priority=1, dataProvider="Invalidtestdata") /* TVO-700 Student Registration Invalid User */
	
	public void StudentRegistrationInvalidUserTest(String Name, String Email, String Password, String OEN, String grade, String stuErrorProp, boolean existsInDB, String ss) throws InterruptedException, IOException {
	    driver.get(testenvironment.url()+"register/student");
		WebDriverWait wait = new WebDriverWait(driver, 10);
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		
		// Verifying Invalid Data for Student Registration
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties.getProperty("usernameField"))));
		GetElement("usernameField").clear();
		GetElement("usernameField").sendKeys(Name);

		GetElement("emailField").clear();
		GetElement("emailField").sendKeys(Email);
		GetElement("passwordField").clear();
		GetElement("passwordField").sendKeys(Password);
		
		GetElement("oenField").clear();
		GetElement("oenField").sendKeys(OEN);

		Select select = new Select(GetElement("selectGradeFieldXpath"));
		select.selectByValue(grade);
		
		if(grade == "3" || grade =="4") 
		{
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='courseCode']")));
			WebElement ele = driver.findElement(By.xpath("//select[@id='courseCode']"));
			js.executeScript("arguments[0].scrollIntoView(true);", ele);
			System.out.println("grade 3 & 4 visible");
			
			
			Select select1 = new Select(GetElement("selectBoardFieldXpath"));
			select1.selectByIndex(0);
			
			if(GetElement("agreementCheckBox").isSelected()) {
				
				GetElement("agreementCheckBox").click();
				
			}
			
		} else if (grade == "1" || grade == "2") {
			ElementClick("selectBoardFieldXpath");
			Select select2 = new Select(GetElement("selectBoardFieldXpath"));
			select2.selectByIndex(46);
			System.out.println("grade 1 & 2 visible"); 
			
			if (!GetElement("agreementCheckBox").isSelected()) {
				
				GetElement("agreementCheckBox").click(); //Agreement Selected
			
			}
				
		} else {
			
			System.out.println("No grade"); //No Grade Selected
			
		}
		
		

	     
	    
		WebElement el = GetElement("submitButton");
		
		js.executeScript("arguments[0].scrollIntoView(true);", el);
		js.executeScript("arguments[0].click();", el);
		
		// Validating error messages.
		if (existsInDB == false) {
		    WebElement nav = GetElement("navheader");
		    js.executeScript("arguments[0].scrollIntoView(true);", nav);
		   
		WebElement ele5 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#studentRegister > div.c-status-update.animated.fadeIn.failure")));
		js.executeScript("arguments[0].scrollIntoView(true);", ele5);
		Thread.sleep(2000);
		String textmsg = ele5.getText();
		
		Assert.assertEquals(textmsg.trim(), ReadProperty(stuErrorProp));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#studentRegister > div.c-status-update.animated.fadeIn.failure.fadeOut")));
		
		}
		else if (existsInDB = true) {
			WebElement ele5 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='error']")));
			String textmsg = ele5.getText();
			Assert.assertEquals(textmsg.trim(), ReadProperty(stuErrorProp));
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#studentRegister > div.c-status-update.animated.fadeIn.failure.fadeOut")));
	       
			} 
		 System.out.println("Success - Student Registration Invalid User program");
		}

	}
