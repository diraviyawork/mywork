package tvo826_whiteboardTools;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import tvo616_LandingPageScenarios.QA_BaseClass;

public class QA_WhiteBoardToolsForAllUser extends QA_BaseClass {

    @Parameters({ "Uname", "Pwd" })
    @Test

    public void whiteBoardToolsStudent(String Uname, String Pwd) throws InterruptedException, IOException {

        driver.get(testenvironment.url() + "login");
        WebDriverWait wait = new WebDriverWait(driver, 5);
        driver.manage().window().setSize(new Dimension(1600, 800));
        Actions action = new Actions(driver);
        JavascriptExecutor js = (JavascriptExecutor) driver;


        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@id='main-content']"))));

      
        GetElement("usernameInput").clear();
        GetElement("usernameInput").sendKeys(Uname);
        GetElement("passInput").clear();
        GetElement("passInput").sendKeys(Pwd);
       
        GetElement("loginButton").click();
       
        System.out.println("Student Logged In - WhiteBoard Tools Program");

        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("landingMainContent"))));

       
        assertEquals(getText("welcomeMessage").toLowerCase(), "welcome, " + Uname.toLowerCase() + "!");


        GetElement("avatarDisplay").isDisplayed();

        GetElement("editProfile").isDisplayed();
        assertEquals(getText("editProfile"), "Edit Profile");
        ElementClick("editProfile");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("editProfilePage"))));
                
        assertEquals(getText("profileText"), "Profile");
        assertEquals(getText("UsernameText"), "Username:");
        assertEquals(getText("name"), Uname);

        ElementClick("editProfileButton");

        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("usernameFullText"))));
                
        assertEquals(getText("usernameFullText"), "Username: " + Uname);

        assertEquals(getText("gradeText"), "Grade");
        Select select = new Select(GetElement("gradeClick"));
        select.selectByValue("2");
        assertEquals(getText("boardText"), "School Board/Organization");
        Select select1 = new Select(GetElement("boardClick"));
        select1.selectByValue("5");
        ElementClick("saveProfile");

        ElementClick("logoNavigation");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("landingMainContent"))));
                
        GetElement("guidedTourButton").isDisplayed();
        GetElement("guidedTourButton").isEnabled();
        GetElement("startSession").isDisplayed();
        GetElement("startSession").isEnabled();
        ElementClick("startSession");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"app\"]/div[2]")));

        
        assertEquals(getText("chooseTutorButton"), "Choose a Tutor");

        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"app\"]/div[2]")));

        Thread.sleep(3000);
        WebElement whiteBoard = driver.findElement(By.cssSelector("#app > div.whiteboards > div:nth-child(1) > div.whiteboard.active > div > canvas.upper-canvas"));
                

        ElementClick("clearAll");
        ElementClick("clearYes");

        action.clickAndHold(GetElement("pencil")).dragAndDropBy(whiteBoard, 10, 50).release().build().perform();


        action.click(GetElement("text")).build().perform();
        action.click(whiteBoard);

        action.moveByOffset(50, 30).click().sendKeys("Text Entered Finally").release().perform();
        action.click(whiteBoard).build().perform();


        action.click(GetElement("shapes")).build().perform();
        action.click(GetElement("triangle"));
        action.click(whiteBoard).build().perform();


        action.click(GetElement("grid")).build().perform();
        action.click(GetElement("parabolaGrid"));
        action.click(whiteBoard).build().perform();


        ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
        WebElement upload = driver.findElement(By.xpath("//input[@title='Image Upload']"));
        js.executeScript("arguments[0].style.visibility='visible';", upload);
        String filePath = System.getProperty("user.dir") + "/src/math.jpg"; 
        upload.sendKeys(filePath);
        
        System.out.println("Added Image in WhiteBoard");
        action.click(GetElement("moveIcon")).build().perform();
       

        action.clickAndHold().dragAndDropBy(whiteBoard, -300, 80).release().build().perform();
      

        action.moveToElement(whiteBoard, 0, 0).click().build().perform();

        action.click(GetElement("shapes")).build().perform();

        action.click(GetElement("hexagon")).moveByOffset(200, 70).release().build().perform();
        action.clickAndHold().dragAndDropBy(whiteBoard, 50, 80).release().build().perform();
        action.moveToElement(whiteBoard, 0, 0).click().build().perform();

        action.click(GetElement("colours")).build().perform();
        action.click(GetElement("purple")).build().perform();

        action.click(GetElement("shapes")).build().perform();

        action.click(GetElement("line")).build().perform();

        action.moveToElement(whiteBoard, 10, 80).clickAndHold().moveByOffset(110, 60).release().build().perform();


        action.click(GetElement("colours")).build().perform();
        action.click(GetElement("red")).build().perform();

        action.click(GetElement("pencil")).build().perform();
        action.moveToElement(whiteBoard, 180, 280).clickAndHold().moveByOffset(280, 120).release().build().perform();

        action.click(GetElement("panel2")).build().perform();

        action.click(GetElement("pencil")).build().perform();
        action.moveToElement(whiteBoard, 180, 280).clickAndHold().moveByOffset(280, 120).moveByOffset(120, 20)
                .moveByOffset(21, 50).release().build().perform();


        action.click(GetElement("text")).build().perform();
        action.click(whiteBoard);

        action.moveByOffset(10, -30).click().sendKeys("Text in Panel2").release().perform();
        action.click(whiteBoard).build().perform();


        action.click(GetElement("undo")).build().perform();
        action.click(whiteBoard);
        action.click(GetElement("redo")).build().perform();
        action.click(whiteBoard);


       

        WebElement save = driver.findElement(By.xpath("//*[@id=\"p4\"]/div[3]/button"));
        action.click(save).build().perform();

        WebElement saveName = driver.findElement(By.xpath("//*[@id=\"input-name\"]"));
        WebElement saveButton = driver.findElement(By.xpath("//*[@id=\"app\"]/div[5]/div/div[4]/div[3]/button"));
        WebElement continueWorking = driver.findElement(By.xpath("//*[@id=\"app\"]/div[5]/div/div[2]/div[2]/button[2]"));
               
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("saveName"))));
        saveName.click();
        saveName.sendKeys("AutoTest1");
        saveButton.click();
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("continueWorking"))));
                
        continueWorking.click();


        ElementClick("userAvatar");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("userMenuOptions"))));
        ElementClick("logOutMenu");
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//section[@id='main-content']//div[@class='container']"))));
        System.out.println("Logged Out from Student - WhiteBoard Tools Program");

    }

    @Parameters({ "Username", "Password" })
    @Test

    public void whiteBoardToolsEducator(String Username, String Password) throws InterruptedException, IOException {

        driver.get(testenvironment.url() + "login");
        WebDriverWait wait = new WebDriverWait(driver, 5);
        driver.manage().window().setSize(new Dimension(1600, 800));
        Actions action = new Actions(driver);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@id='main-content']"))));

        GetElement("usernameInput").clear();
        GetElement("usernameInput").sendKeys(Username);
        GetElement("passInput").clear();
        GetElement("passInput").sendKeys(Password);
        
        GetElement("loginButton").click();
        
        System.out.println("Educator - WhiteBoard Tools Program");
        assertEquals(getText("welcomeMessage").toLowerCase(), "welcome, " + Username.toLowerCase() + "!");


        
        assertEquals(getText("viewQueueButton"), "View Queue");

        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("landingMainContent"))));
        GetElement("avatarDisplay").isDisplayed();

        
        GetElement("editProfile").isDisplayed();
        assertEquals(getText("editProfile"), "Edit Profile");
        ElementClick("editProfile");
       
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("editProfilePage"))));
        assertEquals(getText("profileText"), "Profile");
        assertEquals(getText("UsernameText"), "Username:");
        assertEquals(getText("name"), Username);
        ElementClick("editProfileButton");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("usernameFullText"))));
        assertEquals(getText("usernameFullText"), "Username: " + Username);

        assertEquals(getText("boardTextEdu"), "School Board/Organization");
        Select select1 = new Select(GetElement("boardClick"));
        select1.selectByValue("5");
        ElementClick("saveProfile");

        ElementClick("logoNavigation");

        

        GetElement("guidedTourButton").isDisplayed();
        GetElement("guidedTourButton").isEnabled();
        GetElement("startSession").isDisplayed();
        GetElement("startSession").isEnabled();
        ElementClick("startSession");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"app\"]/div[2]")));

        WebElement whiteBoard = driver.findElement(By.cssSelector("#app > div.whiteboards > div:nth-child(1) > div.whiteboard.active > div > canvas.upper-canvas"));

        
        ElementClick("clearAll");
        ElementClick("clearYes");

        

        action.clickAndHold(GetElement("pencil")).dragAndDropBy(whiteBoard, 10, 50).release().build().perform();


        action.click(GetElement("text")).build().perform();
        action.click(whiteBoard);

        action.moveByOffset(50, 30).click().sendKeys("Text Entered Finally").release().perform();
        action.click(whiteBoard).build().perform();


        action.click(GetElement("shapes")).build().perform();
        action.click(GetElement("triangle"));
        action.click(whiteBoard).build().perform();

        action.click(GetElement("grid")).build().perform();
        action.click(GetElement("parabolaGrid"));
        action.click(whiteBoard).build().perform();


        ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
        WebElement upload = driver.findElement(By.xpath("//input[@title='Image Upload']"));
        js.executeScript("arguments[0].style.visibility='visible';", upload);

        String filePath = System.getProperty("user.dir") + "/src/math.jpg"; 
        upload.sendKeys(filePath);

        action.click(GetElement("moveIcon")).build().perform();

        action.clickAndHold().dragAndDropBy(whiteBoard, -300, 80).release().build().perform();


        action.moveToElement(whiteBoard, 0, 0).click().build().perform();


        action.click(GetElement("shapes")).build().perform();

        action.click(GetElement("hexagon")).moveByOffset(200, 70).release().build().perform();
        action.clickAndHold().dragAndDropBy(whiteBoard, 50, 80).release().build().perform();
        action.moveToElement(whiteBoard, 0, 0).click().build().perform();

        action.click(GetElement("colours")).build().perform();
        action.click(GetElement("purple")).build().perform();

        action.click(GetElement("shapes")).build().perform();

        action.click(GetElement("line")).build().perform();

        action.moveToElement(whiteBoard, 10, 80).clickAndHold().moveByOffset(110, 60).release().build().perform();

        action.click(GetElement("colours")).build().perform();
        action.click(GetElement("red")).build().perform();

        action.click(GetElement("pencil")).build().perform();
        action.moveToElement(whiteBoard, 180, 280).clickAndHold().moveByOffset(280, 120).release().build().perform();

        action.click(GetElement("panel2")).build().perform();

        action.click(GetElement("pencil")).build().perform();
        action.moveToElement(whiteBoard, 180, 280).clickAndHold().moveByOffset(280, 120).moveByOffset(120, 20)
                .moveByOffset(21, 50).release().build().perform();


        action.click(GetElement("text")).build().perform();
        action.click(whiteBoard);

        action.moveByOffset(10, -30).click().sendKeys("Text in Panel2").release().perform();
        action.click(whiteBoard).build().perform();


        action.click(GetElement("undo")).build().perform();
        action.click(whiteBoard);
        action.click(GetElement("redo")).build().perform();
        action.click(whiteBoard);
        action.clickAndHold().dragAndDropBy(whiteBoard, -300, 104).release().build().perform();

        

        WebElement save = driver.findElement(By.xpath("//*[@id=\"p4\"]/div[3]/button"));
        action.click(save).build().perform();

        WebElement saveName = driver.findElement(By.xpath("//*[@id=\"input-name\"]"));
        WebElement saveButton = driver.findElement(By.xpath("//*[@id=\"app\"]/div[5]/div/div[4]/div[3]/button"));
        WebElement continueWorking = driver.findElement(By.xpath("//*[@id=\"app\"]/div[5]/div/div[2]/div[2]/button[2]"));
        Thread.sleep(3000);
  
        saveName.click();
        saveName.sendKeys("AutoTest1");
        saveButton.click();
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("continueWorking"))));
        continueWorking.click();


        ElementClick("userAvatar");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("userMenuOptions"))));
        ElementClick("logOutMenu");
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//section[@id='main-content']//div[@class='container']"))));
        System.out.println("Logged Out and Success WhiteBoard Tools Program");

    }

}
