package tvo607_LoginScenarios;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import tvo616_LandingPageScenarios.QA_BaseClass;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertEquals;

public class QA_LoginInvalidUser extends QA_BaseClass {

    
    @Test(dataProvider = "LoginInvalidUser", priority = 1)
    public void invalidUser(String username, String password) throws InterruptedException, IOException{
        
        driver.get(testenvironment.url());
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties.getProperty("loginTopLink"))));
        GetElement("loginTopLink").click();
        GetElement("usernameInput").sendKeys(username);
        GetElement("passInput").sendKeys(password);
        GetElement("loginButton").click();
        Thread.sleep(2000);


        if (username == "" && password == "") {

            WebElement usernameError = GetElement("usernameErrorMsg");
            
                assertEquals(usernameError.getText(), "Please enter Email\nPlease enter Password");

        } else if (password == "")
        {
            WebElement passwordError = GetElement("passwordErrorMsg");
            assertEquals(passwordError.getText(), "Please enter Password");
           
        }
        else if (username == ""){
            WebElement usernameError = GetElement("usernameErrorMsg");
            assertEquals(usernameError.getText(), "Please enter Email");
            
        }
        else if (username == "Test1" && password =="Test123!"){
            WebElement noactiveError = GetElement("noactiveErrorMsg");
            assertEquals(noactiveError.getText(), "Please activate your account first.");
        }
        else {
            WebElement Error = GetElement("errorData");
            assertEquals(Error.getText(), "Invalid username/email.");
            
        }
        System.out.println("Success - Login Invalid User Program");

    }

    @DataProvider(name = "LoginInvalidUser")

    public Object[][] testData() {
        return new Object[][]{
                {"", ""},
                {"vangjush", ""},
                {"", "admin"},
                {"Test1", "Test123!"},
                {"invaliduser", "invalidpass"},
                {"invalid", "invalid123"}

        };
    }
}

