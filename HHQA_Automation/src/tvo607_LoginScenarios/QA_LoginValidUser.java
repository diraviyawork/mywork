package tvo607_LoginScenarios;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import tvo616_LandingPageScenarios.QA_BaseClass;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class QA_LoginValidUser extends QA_BaseClass {

    @Test(dataProvider = "LoginValidUser")
    public void validUser(String username, String password, String expURL) throws InterruptedException, IOException {
        
       
        driver.get(testenvironment.url()+"login");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties.getProperty("loginTopLink"))));
        
       
        GetElement("usernameInput").clear();

        GetElement("usernameInput").sendKeys(username);
        GetElement("passInput").clear();

        GetElement("passInput").sendKeys(password);

        GetElement("visibleIcone").click();
    
        if (GetElement("passInput").getAttribute("type").equals("text")){

            System.out.println("Password visible");
  
        }
        else {
            System.out.println("Password is not visible");
         
        }
        screenShot(driver, "submit");
        GetElement("loginButton").click();
        Thread.sleep(2000);

        String url = driver.getCurrentUrl();
        
        assertEquals(url, expURL);
        
        
        if (username == "admin" && password == "admin") {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties.getProperty("adminWrapper"))));
            assertEquals(getText("usersAdmin"), "Users");
            WebElement adDropdownUp=driver.findElement(By.xpath("//div[@class='visible-lg visible-md visible-sm c-admin-nav__control']//div[@class='c-user-icon']"));
            WebElement adminLogout=driver.findElement(By.xpath("//div[@class='visible-lg visible-md visible-sm c-admin-nav__control']//a[@title='Logout'][contains(text(),'Logout')]"));
            
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView();", adDropdownUp);
            js.executeScript("arguments[0].click();", adDropdownUp);
            js.executeScript("arguments[0].scrollIntoView();", adminLogout);
            js.executeScript("arguments[0].click();", adminLogout);
            
            
          
            
        }
        else  {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties.getProperty("landingOtherUsers"))));
            assertEquals(getText("startSession"), "Start Session");
            ElementClick("userAvatar");
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("userMenuOptions"))));
            ElementClick("logOutMenu");
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//section[@id='main-content']//div[@class='container']"))));
            System.out.println("Logged Out - Login Valid User Program");
        }
        System.out.println("Success - Login Valid User Program");
        }
    
    @DataProvider(name = "LoginValidUser")

    public Object[][] passData () {
        return new Object[][]{
                {"admin","admin", "https://tvo-qa.com/admin"},
                {"vangjush@skooli.com","admin", "https://tvo-qa.com/whiteboard/whiteboard-landing-page"},
                {"vangjushedu","admin", "https://tvo-qa.com/whiteboard/whiteboard-landing-page"},
                {"vangjo", "admin", "https://tvo-qa.com/whiteboard/whiteboard-landing-page"},
                {"Lorenc", "Test123!", "https://tvo-qa.com/whiteboard/whiteboard-landing-page"}
        };
    }
}