package tvo607_LoginScenarios;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import tvo616_LandingPageScenarios.QA_BaseClass;


public class QA_LoginLandingPage extends QA_BaseClass {

    String[] str = new String[100];
    String redirectURL = null;
    String[] expectedURL = {"https://tvo-test.com/login", "https://tvo-qa.com/reset/request"};

    @Test(priority = 1)  /* TVO-608  Login Landing Page */
    public void LoginPageTest() throws InterruptedException, IOException {

        //Login Page and navbar-header
        driver.get(testenvironment.url());
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties.getProperty("loginTopLink"))));
        GetElement("loginTopLink").click();
        
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(properties.getProperty("loginText")))));
        GetElement("tvoLogo").isDisplayed();
        GetElement("loginText").isDisplayed();
        
        GetElement("studentTopLink").isDisplayed();
        GetElement("educatorTopLink").isDisplayed();
        GetElement("parentsTopLink").isDisplayed();
        assertEquals(getText("registerTopLink"), "Register");
        assertEquals(getText("loginTopLink"), "Login");
       

        assertTrue(VerifyImage("loginImg"));
        assertEquals(getText("loginText"), "Login");
        

        // Login form
        assertEquals(getText("usernameEmail"), "Username/Email");
        GetElement("usernameInput").isEnabled();
        GetElement("usernameInput").sendKeys("Test");
        assertEquals(getText("password"), "Password");
        GetElement("passInput").isEnabled();
        GetElement("passInput").sendKeys("test");
        GetElement("ForgotUserPass").isDisplayed();
        assertEquals(getText("loginButton"), "Login");
        GetElement("loginButton").isDisplayed();
        GetElement("loginButton").isEnabled();
        System.out.println("Login Form - Login Landing Page Success");

        // Footer
        GetElement("footerMain").isDisplayed();
        GetElement("footerSub").isDisplayed();
        System.out.println("Success - LoginLanding Page program");
    }

    // Verify link and page for Forgot username/password

    @Test(priority = 2) //redirection of Forgot username/password Link
    public void ForgotPassword () throws InterruptedException, IOException {
        WebDriverWait wait2 = new WebDriverWait(driver, 3);
        System.out.println("Username and Password Change Request");
        ElementClick("ForgotUserPass");
        redirectURL = driver.getCurrentUrl();
        Assert.assertEquals(redirectURL, expectedURL[1]);
        

        if (redirectURL.equals(expectedURL[1])) {

            wait2.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("passRequest"))));
            assertEquals(getText("changePasswordTxt"), "Username and Password Change Request");
            

        }
    }
    
}

