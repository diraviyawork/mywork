package tvo616_LandingPageScenarios;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import Configuration.Environment;

public class QA_BaseClass {

    protected WebDriver                        driver;
    String                                     baseURL;
    public static Properties                   properties;
    public static Properties                   TextProperties;
    public static ThreadLocal<RemoteWebDriver> dr = new ThreadLocal<RemoteWebDriver>();
    protected Environment                      testenvironment;

    @BeforeTest
    @Parameters(value = { "myBrowser", "environment" })
    public void beforeClass(String myBrowser, String environment) throws IOException {

        // nodeURL = "http://0.0.0.0:4444/wd/hub"; NodeURL is mentioned here for a
        // reference, nothing much
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("browserName", myBrowser);
        capability.setCapability("marionette", true);
        driver = new RemoteWebDriver(new URL("http://0.0.0.0:4444/wd/hub"), capability);
        
        properties = new Properties();
        TextProperties = new Properties();
        Thread currentThread = Thread.currentThread();
        ClassLoader contextClassLoader = currentThread.getContextClassLoader();
        try {
            InputStream propertiesStream = contextClassLoader.getResourceAsStream("./Configuration/config.properties");
            if (propertiesStream != null) {
                properties.load(propertiesStream);
                // TODO close the stream
            } else {
                System.out.println("Properties file not found!");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ClassLoader contextClassLoader1 = currentThread.getContextClassLoader();
        try {
            InputStream propertiesStream1 = contextClassLoader1
                    .getResourceAsStream("./Configuration/TextSource.properties");
            if (propertiesStream1 != null) {
                TextProperties.load(propertiesStream1);
                // TODO close the stream
            } else {
                System.out.println("Properties file not found!");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ConfigFactory.setProperty("env", environment);
        testenvironment = ConfigFactory.create(Environment.class);

    }

    public WebDriver getDriver() {
        return dr.get();
    }

    public void setWebDriver(RemoteWebDriver driver) {
        dr.set(driver);
    }

    public String FindElements(String EleXpath) {
        WebElement E = driver.findElement(By.xpath(properties.getProperty(EleXpath)));
        // JavascriptExecutor js = (JavascriptExecutor) driver;
        // js.executeScript("arguments[0].scrollIntoView();", E);
        return E.getText();

    }

    public String ReadProperty(String Prop) {

        String P1 = TextProperties.getProperty(Prop);
        return P1;

    }

    public void ElementClick(String EleX) {
        WebElement E1 = driver.findElement(By.xpath(properties.getProperty(EleX)));
        /*
         * Below lines are commented as few elements are not scrolling to the point and
         * works good without JS. Please uncomment these lines if it is needed for
         * scrolling to a particular point
         */
        // JavascriptExecutor js = (JavascriptExecutor) driver;
        // js.executeScript("arguments[0].scrollIntoView();", E1);
        E1.click();
        return;
    }

    public String getText(String text) {

        String TextPath = driver.findElement(By.xpath(properties.getProperty(text))).getText();

        return TextPath;

    }

    @SuppressWarnings("resource")
    public String getParagraphFromFile(int IndexNum) {

        File file = new File("./HHQA_Automation/ActualParagraphs.txt");
        ArrayList<String> arraypara = new ArrayList<String>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            Scanner sc = new Scanner(br).useDelimiter("\n");
            while (sc.hasNext()) {
                String paragraph = sc.next();
                arraypara.add(paragraph);
            }
            sc.close();

        } catch (Exception e) {
            System.out.println(e);
        }
        return arraypara.get(IndexNum);

    }

    public boolean CompareStrings(String ActualStr, String ExpectedStr) {
        try {
            Assert.assertEquals(ActualStr, ExpectedStr);
        } catch (Throwable t) {
            Assert.fail("Strings Do not match"); // Hard assertion -> Stops your test on assertion failure
            return true;
        }
        return false;
    }

    public String RedirectLink(String path, String linkpath) {
        String sourceLink = (String) ((JavascriptExecutor) driver).executeScript(properties.getProperty(path),
                linkpath(linkpath));

        return sourceLink;

    }

    public WebElement linkpath(String linkText) {
        WebElement linksrc = driver.findElement(By.linkText(linkText));
        return linksrc;
    }

    public boolean VerifyImage(String ImgPath) {
        WebElement E2 = driver.findElement(By.xpath(properties.getProperty(ImgPath)));
        Boolean ImgPresence = (Boolean) ((JavascriptExecutor) driver).executeScript(
                "return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0",
                E2);
        return ImgPresence;

    }

    public boolean LinkVerification(String LinkText1) {

        WebElement E3 = driver.findElement(By.xpath(properties.getProperty(LinkText1)));
        Boolean test = (Boolean) ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView()", E3);
        return test;

    }

    public WebElement GetElement(String XPath) {
        WebElement E3 = driver.findElement(By.xpath(properties.getProperty(XPath)));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", E3);
        return E3;

    }

    public static String screenShot(WebDriver driver, String name) throws IOException{

        File scr = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        String path = "/Users/diya/Images/" +  name +".png";
        File dest = new File(path);
        FileUtils.copyFile(scr, dest);
        return path;
    }

    @AfterTest
    public void teardown() throws InterruptedException {
        Thread.sleep(10000); // Added Implicit wait here, as some scripts aren't working in firefox due to
                             // marionette issue. Comment it if you don't need this for your script/execution
        driver.quit();
    }
}
