package tvo616_LandingPageScenarios;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;


public class QA_PrimaryLandingPage extends QA_BaseClass  {

	String[] str = new String[100];
 	String redirectURL= null;
	String[] expectedURL = { "https://tvo.org/education" , "https://secure3.convio.net/tvo/site/SSurvey?SURVEY_ID=1020&ACTION_REQUIRED=URI_ACTION_USER_REQUESTS" };
	String[] linkFoo = new String[50];
	
@Test(priority=1)  /* TVO617 - LandingPage */
public void StudentTest() throws InterruptedException, IOException {
	
		driver.get(testenvironment.url()); 
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//section[@class='c-hero']//div[@class='container']"))));
		GetElement("tvoLogo").isDisplayed();
		System.out.println("pass");
		GetElement("studentTopLink").isDisplayed();
		GetElement("educatorTopLink").isDisplayed();
		GetElement("parentsTopLink").isDisplayed();
		assertEquals(getText("registerTopLink"), "Register");
		assertEquals(getText("loginTopLink"), "Login");
		GetElement("activeEnable").isDisplayed();
			
			assertTrue(VerifyImage("studentHeroImg"));
			assertEquals(getText("studentCenterText"), "Welcome to Your Personal Math Mentor");
			System.out.println("Student Landing Page - Primary Landing Page Program");
			ElementClick("arrowDownStud");
			assertTrue(VerifyImage("studentIntroImg"));
			str[1] = FindElements("studTextHeader1Xpath");
			CompareStrings(str[1], ReadProperty("studTextHeader1"));
			str[2] = FindElements("studPara1Xpath");
			CompareStrings(str[2], ReadProperty("studPara1"));
			str[3] = FindElements("studPara2Xpath");
			CompareStrings(str[3], ReadProperty("studPara2"));
			str[4] = FindElements("studPara3Xpath");
			CompareStrings(str[4], ReadProperty("studPara3"));
			assertTrue(VerifyImage("featureCalendarImg"));
			str[5] = FindElements("studFeature1Xpath");
			CompareStrings(str[5], ReadProperty("studFeature1"));
			assertTrue(VerifyImage("featureChatImg"));
			str[6] = FindElements("studFeature2Xpath");
			CompareStrings(str[6], ReadProperty("studFeature2"));
			assertTrue(VerifyImage("featureWhiteboardImg"));
			str[7] = FindElements("studFeature3Xpath");
			CompareStrings(str[7], ReadProperty("studFeature3"));
			assertTrue(VerifyImage("featureLockerImg"));
			str[8] = FindElements("studFeature4Xpath");
			CompareStrings(str[8], ReadProperty("studFeature4"));
			assertTrue(VerifyImage("featureSecureImg"));
			str[9] = FindElements("studFeature5Xpath");
			CompareStrings(str[9], ReadProperty("studFeature5"));
			assertTrue(VerifyImage("featureCompatibleImg"));
			str[10] = FindElements("studFeature6Xpath");
			GetElement("separatorLine").isDisplayed();
			CompareStrings(str[10], ReadProperty("studFeature6"));
			str[11] = FindElements("studGetStartedXpath");
			CompareStrings(str[11], ReadProperty("studGetStarted"));
			str[12] = FindElements("studentsSayingHeaderXpath");
			CompareStrings(str[12], ReadProperty("studentsSayingHeader"));
			assertTrue(VerifyImage("avatarStudent001"));
			str[13] = FindElements("studSaying1Xpath");
			CompareStrings(str[13], ReadProperty("studSaying1"));
			str[14] = FindElements("stud1GradeXpath");
			CompareStrings(str[14], ReadProperty("stud1Grade"));
			assertTrue(VerifyImage("avatarStudent010"));
			str[15] = FindElements("studSaying2Xpath");
			CompareStrings(str[15], ReadProperty("studSaying2"));
			str[16] = FindElements("stud2GradeXpath");
			CompareStrings(str[16], ReadProperty("stud2Grade"));
			assertTrue(VerifyImage("avatarStudent082"));
			str[17] = FindElements("studSaying3Xpath");
			CompareStrings(str[17], ReadProperty("studSaying3"));
			str[18] = FindElements("stud3GradeXpath");
			CompareStrings(str[18], ReadProperty("stud3Grade"));
			assertTrue(VerifyImage("avatarStudent048"));
			
			str[19] = FindElements("studSaying4Xpath");
			CompareStrings(str[19], ReadProperty("studSaying4"));
			str[20] = FindElements("stud4GradeXpath");
			CompareStrings(str[20], ReadProperty("stud4Grade"));
			
			assertEquals(getText("quickLinkFooterText"), "Quick Links");
			GetElement("studentQuickLink").isDisplayed();
			GetElement("educatorQuickLink").isDisplayed();
			GetElement("parentsQuickLink").isDisplayed();
			GetElement("loginQuickLink").isDisplayed();
			GetElement("aboutUsQuickLink").isDisplayed();
			GetElement("contactUsQuickLink").isDisplayed();
			assertEquals(getText("moreResourcesFooterText"), "More Resources from TVO");
			driver.findElement(By.linkText("TVO")).isDisplayed();
			GetElement("tvoKidsQuickLink").isDisplayed();
			GetElement("mPowerQuickLink").isDisplayed();
			GetElement("iLCQuickLink").isDisplayed();
			GetElement("gedTestingQuickLink").isDisplayed();
			GetElement("newsLetterFooter").isDisplayed();
			GetElement("termsOfServiceFooter").isDisplayed();
			GetElement("privacyPolicyFooter").isDisplayed();
			GetElement("facebookIconFooter").isDisplayed();
			GetElement("twitterIconFooter").isDisplayed();
			GetElement("subscribeNewsLetterIconFooter").isDisplayed();
			GetElement("subscribeNewsLetterFooter").isDisplayed();
			GetElement("copyRightsFooter").isDisplayed();
			assertEquals(getText("subscribeNewsLetterFooter"), ReadProperty("subscribeNewsLetterText"));
			assertEquals(getText("copyRightsFooter"), ReadProperty("copyRightsText"));
			
			
	}
	
	@Test (priority=2)
	public void EducatorTest() throws InterruptedException, IOException {
			
			System.out.println("Educator Landing Page - Primary Landing Page Program");
			ElementClick("educatorTopLink");
			GetElement("tvoLogo").isDisplayed();
			GetElement("activeEnable").isDisplayed();
			GetElement("educatorTopLink").isDisplayed();
			GetElement("parentsTopLink").isDisplayed();
			GetElement("studentTopLink").isDisplayed();
			assertEquals(getText("registerTopLink"), "Register");
			assertEquals(getText("loginTopLink"), "Login");
			
			assertTrue(VerifyImage("educatorHeroImg"));
			assertEquals(getText("educatorCenterText"), "Bring Mathify into Your Classroom!");
			ElementClick("arrowDownEdu");
			assertTrue(VerifyImage("educatorIntroImg"));
			str[21] = FindElements("eduTextHeader1Xpath");
			CompareStrings(str[21], ReadProperty("eduTextHeader1"));
			str[22] = FindElements("eduPara1Xpath");
			CompareStrings(str[22], ReadProperty("eduPara1"));
			str[23] = FindElements("eduPara2Xpath");
			CompareStrings(str[23], ReadProperty("eduPara2"));
			assertTrue(VerifyImage("featureMathImg"));
			str[24] = FindElements("eduFeature1Xpath");
			CompareStrings(str[24], ReadProperty("eduFeature1"));
			assertTrue(VerifyImage("featureWhiteboardImg"));
			str[25] = FindElements("eduFeature2Xpath");
			CompareStrings(str[25], ReadProperty("eduFeature2"));
			assertTrue(VerifyImage("featureLockerImg"));
			str[26] = FindElements("eduFeature3Xpath");
			CompareStrings(str[26], ReadProperty("eduFeature3"));
			assertTrue(VerifyImage("featureEndorsedImg"));
			str[27] = FindElements("eduFeature4Xpath");
			CompareStrings(str[27], ReadProperty("eduFeature4"));
			assertTrue(VerifyImage("featureSupportImg"));
			str[28] = FindElements("eduFeature5Xpath");
			CompareStrings(str[28], ReadProperty("eduFeature5"));
			assertTrue(VerifyImage("featureCompatibleImg"));
			str[29] = FindElements("eduFeature6Xpath");
			CompareStrings(str[29], ReadProperty("eduFeature6"));
			GetElement("separatorLine").isDisplayed();
			str[30] = FindElements("eduGetStartedXpath");
			CompareStrings(str[30], ReadProperty("eduGetStarted"));
			assertTrue(VerifyImage("checkMarkImg1"));
			assertEquals(getText("inClassResourcesText"), "In-Class Resources");
			assertTrue(VerifyImage("checkMarkImg2"));
			assertEquals(getText("interactivesText"), "Interactives");
			assertTrue(VerifyImage("checkMarkImg3"));
			assertEquals(getText("videosText"), "Videos");
			assertTrue(VerifyImage("checkMarkImg4"));
			assertEquals(getText("mathGlossaryText"), "Math glossary");
			GetElement("separatorLine").isDisplayed();
			
			str[31] = FindElements("eduContactTextXpath");
			CompareStrings(str[31], ReadProperty("eduContactText"));
			assertEquals(getText("schoolVisitText"), "A school visit");
			assertEquals(getText("promotionalMaterialText"),"Promotional material for your school");
			assertEquals(getText("localAmbassadorText"), "Connect with your local (HH) ambassador");
			GetElement("contactUsEducatorButton").isEnabled();
			assertEquals(getText("contactUsEducatorButton"), "Contact Us");
			str[32] = FindElements("eduSayingHeaderXpath");
			CompareStrings(str[32], ReadProperty("eduSayingHeader"));
			assertTrue(VerifyImage("avatarStudent048"));
			str[33] = FindElements("eduSaying1Xpath");
			CompareStrings(str[33], ReadProperty("eduSaying1"));
			str[34] = FindElements("edu1GradeXpath");
			CompareStrings(str[34], ReadProperty("edu1Grade"));
			assertTrue(VerifyImage("avatarStudent082"));
			str[35] = FindElements("eduSaying2Xpath");
			CompareStrings(str[35], ReadProperty("eduSaying2"));
			str[36] = FindElements("edu2GradeXpath");
			CompareStrings(str[36], ReadProperty("edu2Grade"));
			assertTrue(VerifyImage("avatarStudent001"));
			str[37] = FindElements("eduSaying3Xpath");
			CompareStrings(str[37], ReadProperty("eduSaying3"));
			str[38] = FindElements("edu3GradeXpath");
			CompareStrings(str[38], ReadProperty("edu3Grade"));
			assertTrue(VerifyImage("avatarStudent050"));
			str[39] = FindElements("eduSaying4Xpath");
			CompareStrings(str[39], ReadProperty("eduSaying4"));
			str[40] = FindElements("edu4GradeXpath");
			CompareStrings(str[40], ReadProperty("edu4Grade"));
			
			assertEquals(getText("quickLinkFooterText"), "Quick Links");
			GetElement("studentQuickLink").isDisplayed();
			GetElement("educatorQuickLink").isDisplayed();
			GetElement("parentsQuickLink").isDisplayed();
			GetElement("loginQuickLink").isDisplayed();
			GetElement("aboutUsQuickLink").isDisplayed();
			GetElement("contactUsQuickLink").isDisplayed();
			assertEquals(getText("moreResourcesFooterText"), "More Resources from TVO");
			driver.findElement(By.linkText("TVO")).isDisplayed();
			GetElement("tvoKidsQuickLink").isDisplayed();
			GetElement("mPowerQuickLink").isDisplayed();
			GetElement("iLCQuickLink").isDisplayed();
			GetElement("gedTestingQuickLink").isDisplayed();
			GetElement("newsLetterFooter").isDisplayed();
			GetElement("termsOfServiceFooter").isDisplayed();
			GetElement("privacyPolicyFooter").isDisplayed();
			GetElement("facebookIconFooter").isDisplayed();
			GetElement("twitterIconFooter").isDisplayed();
			GetElement("subscribeNewsLetterIconFooter").isDisplayed();
			GetElement("subscribeNewsLetterFooter").isDisplayed();
			GetElement("copyRightsFooter").isDisplayed();
			assertEquals(getText("subscribeNewsLetterFooter"), ReadProperty("subscribeNewsLetterText"));
			assertEquals(getText("copyRightsFooter"), ReadProperty("copyRightsText"));
		
	} 
	
@Test (priority=3)
public void ParentsTest() throws InterruptedException, IOException {
		
			System.out.println("Parent Landing Page - Primary Landing Page Program");
			ElementClick("parentsTopLink");
			
			GetElement("tvoLogo").isDisplayed();
			GetElement("studentTopLink").isDisplayed();
			GetElement("educatorTopLink").isDisplayed();
			assertEquals(getText("registerTopLink"), "Register");
			assertEquals(getText("loginTopLink"), "Login");
			assertTrue(VerifyImage("parentHeroImg"));
			assertEquals(getText("parentsCenterText"), "Boost Your Child’s Understanding and Confidence in Math!");
			ElementClick("arrowDownParents");
			assertTrue(VerifyImage("educatorIntroImg"));
			str[41] = FindElements("parentsTextHeader1Xpath");
			CompareStrings(str[41], ReadProperty("parentsTextHeader"));
			str[42] = FindElements("parentsPara1Xpath");
			CompareStrings(str[42], ReadProperty("parentsPara1"));
			str[43] = FindElements("parentsPara2Xpath");
			CompareStrings(str[43], ReadProperty("parentsPara2"));
			assertTrue(VerifyImage("featureCalendarImg"));
			str[44] = FindElements("parentsFeature1Xpath");
			CompareStrings(str[44], ReadProperty("parentsFeature1"));
			assertTrue(VerifyImage("featureSupportImg"));
			str[45] = FindElements("parentsFeature2Xpath");
			CompareStrings(str[45], ReadProperty("parentsFeature2"));
			assertTrue(VerifyImage("featureMathImg"));
			str[46] = FindElements("parentsFeature3Xpath");
			CompareStrings(str[46], ReadProperty("parentsFeature3"));
			assertTrue(VerifyImage("featureSecureImg"));
			str[47] = FindElements("parentsFeature4Xpath");
			CompareStrings(str[47], ReadProperty("parentsFeature4"));
			assertTrue(VerifyImage("featureChatImg"));
			str[48] = FindElements("parentsFeature5Xpath");
			CompareStrings(str[48], ReadProperty("parentsFeature5"));
			assertTrue(VerifyImage("featureCompatibleImg"));
			str[49] = FindElements("parentsFeature6Xpath");
			CompareStrings(str[49], ReadProperty("parentsFeature6"));
			GetElement("separatorLine").isDisplayed();
			str[50] = FindElements("parentsGetStartedXpath");
			CompareStrings(str[50], ReadProperty("parentsGetStarted"));
			str[51] = FindElements("parentsSayingHeaderXpath");
			CompareStrings(str[51], ReadProperty("parentsSayingHeader"));
			assertTrue(VerifyImage("avatarStudent082"));
			str[52] = FindElements("parentsSaying1Xpath");
			CompareStrings(str[52], ReadProperty("parentsSaying1"));
			str[53] = FindElements("parents1GradeXpath");
			CompareStrings(str[53], ReadProperty("parents1Grade"));
			assertTrue(VerifyImage("avatarStudent002"));
			str[54] = FindElements("parentsSaying2Xpath");
			CompareStrings(str[54], ReadProperty("parentsSaying2"));
			str[55] = FindElements("parents2GradeXpath");
			CompareStrings(str[55], ReadProperty("parents2Grade"));
			assertTrue(VerifyImage("avatarStudent050"));
			str[56] = FindElements("parentsSaying3Xpath");
			CompareStrings(str[56], ReadProperty("parentsSaying3"));
			str[57] = FindElements("parents3GradeXpath");
			CompareStrings(str[57], ReadProperty("parents3Grade"));
			assertTrue(VerifyImage("avatarStudent048"));
			str[58] = FindElements("parentsSaying4Xpath");
			CompareStrings(str[58], ReadProperty("parentsSaying4"));
			str[59] = FindElements("parents4GradeXpath");
			CompareStrings(str[59], ReadProperty("parents4Grade"));
			assertEquals(getText("quickLinkFooterText"), "Quick Links");
			GetElement("studentQuickLink").isDisplayed();
			GetElement("educatorQuickLink").isDisplayed();
			GetElement("parentsQuickLink").isDisplayed();
			GetElement("loginQuickLink").isDisplayed();
			GetElement("aboutUsQuickLink").isDisplayed();
			GetElement("contactUsQuickLink").isDisplayed();
			assertEquals(getText("moreResourcesFooterText"), "More Resources from TVO");
			driver.findElement(By.linkText("TVO")).isDisplayed();
			GetElement("tvoKidsQuickLink").isDisplayed();
			GetElement("mPowerQuickLink").isDisplayed();
			GetElement("iLCQuickLink").isDisplayed();
			GetElement("gedTestingQuickLink").isDisplayed();
			GetElement("newsLetterFooter").isDisplayed();
			GetElement("termsOfServiceFooter").isDisplayed();
			GetElement("privacyPolicyFooter").isDisplayed();
			GetElement("facebookIconFooter").isDisplayed();
			GetElement("twitterIconFooter").isDisplayed();
			GetElement("subscribeNewsLetterIconFooter").isDisplayed();
			GetElement("subscribeNewsLetterFooter").isDisplayed();
			GetElement("copyRightsFooter").isDisplayed();
			assertEquals(getText("subscribeNewsLetterFooter"), ReadProperty("subscribeNewsLetterText"));
			assertEquals(getText("copyRightsFooter"), ReadProperty("copyRightsText"));

}
	

@Test(priority=4) //Redirection of Footer Links
public void FooterLinks() throws InterruptedException, IOException {
			
			WebDriverWait wait2 = new WebDriverWait(driver, 3);
			
			/* Redirection of Educator Link in Footer */
	
			ElementClick("educatorTopLink");
			
			redirectURL = driver.getCurrentUrl();
			Assert.assertEquals(redirectURL,testenvironment.url()+"/educators");
			System.out.println("Educator Footer Link - Primary Landing Page Program");
			if (redirectURL.equals(testenvironment.url()+"/educators")) {
			
			wait2.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("educatorCenterText"))));
			assertEquals(getText("educatorCenterText"), "Bring Mathify into Your Classroom!");
			GetElement("activeEnable").isDisplayed();
			
			
			}


			/* Redirection of Parent Link in Footer */
			
			ElementClick("parentsTopLink");
			redirectURL = driver.getCurrentUrl();
			Assert.assertEquals(redirectURL,testenvironment.url()+"/parents");
			System.out.println("Parent Footer Link - Primary Landing Page Program");
			if (redirectURL.equals(testenvironment.url()+"/parents")) {
				
			wait2.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("parentsCenterText"))));
			assertEquals(getText("parentsCenterText"), "Boost Your Child’s Understanding and Confidence in Math!");
			GetElement("activeEnable").isDisplayed();
			
			
			}


			/* Redirection of Login Link in Footer */
			
			ElementClick("loginTopLink");
			redirectURL = driver.getCurrentUrl();
			Assert.assertEquals(redirectURL, testenvironment.url()+"/login");
			System.out.println("Login Footer Link - Primary Landing Page Program");
			if (redirectURL.equals(testenvironment.url()+"/login")) {
			
			wait2.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(properties.getProperty("loginCenterBody"))));
			assertEquals(getText("loginText"), "Login");
			wait2.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(properties.getProperty("activeEnable"))));
			
			
			}


			/* Redirection of About Us Link in Footer */
			
			ElementClick("aboutUsQuickLink ");
			redirectURL = driver.getCurrentUrl();
			Assert.assertEquals(redirectURL, testenvironment.url()+"/about-us");
			System.out.println("About Us Footer Link - Primary Landing Page Program");
			if (redirectURL.equals(testenvironment.url()+"/about-us")) {
			
			GetElement("aboutUsCenterBody").isDisplayed();
			assertEquals(getText("aboutUsText"), "About Us");
			wait2.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(properties.getProperty("activeEnable"))));
			
			
			}


			/* Redirection of Contact Us Link in Footer */
			
			ElementClick("contactUsQuickLink");
			redirectURL = driver.getCurrentUrl();
			Assert.assertEquals(redirectURL, testenvironment.url()+"/contact-us");
			System.out.println("Contact Us Footer Link - Primary Landing Page Program");
			if (redirectURL.equals(testenvironment.url()+"/contact-us")) {
			
			GetElement("contactUsCenterBody").isDisplayed();
			assertEquals(getText("contactUsText"), "Contact Us");
			wait2.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(properties.getProperty("activeEnable"))));
			
			}


			/* Redirection of Students Link in Footer */
			
			ElementClick("studentQuickLink");
			redirectURL = driver.getCurrentUrl();
			
			//System.out.println(testenvironment.url());
			Assert.assertEquals(redirectURL,testenvironment.url()+"/");
			System.out.println("Student Footer Link- Primary Landing Page Program");
			if (redirectURL.equals(testenvironment.url())) {
			
			GetElement("activeEnable").isEnabled();
			GetElement("studentCenterBody").isDisplayed();
			assertEquals(getText("studentCenterText"), "Welcome to Your Personal Math Mentor");
			
			}

			
			/* Redirection of Terms of Use Link in Footer */
			
			linkFoo[0] = RedirectLink("targetXpath", "Terms of Use");
			assertEquals(linkFoo[0], properties.getProperty("footerLinkTarget"));
			
			linkFoo[1] = RedirectLink("hrefXpath", "Terms of Use");
			assertEquals(linkFoo[1], properties.getProperty("termsHref"));


			/* Redirection of Privacy Policy Link in Footer */
			
			
			linkFoo[2] = RedirectLink("targetXpath", "Privacy Policy");
			assertEquals(linkFoo[2], properties.getProperty("footerLinkTarget"));
			
			linkFoo[3] = RedirectLink("hrefXpath", "Privacy Policy");
			assertEquals(linkFoo[3], properties.getProperty("privacyHref"));
			
			/* Redirection of TVO Link in Footer */
	
			linkFoo[4] = RedirectLink("targetXpath", "TVO");
			assertEquals(linkFoo[4], properties.getProperty("footerLinkTarget"));
			
			linkFoo[5] = RedirectLink("hrefXpath", "TVO");
			assertEquals(linkFoo[5], properties.getProperty("tvoHref"));
		

			/* Redirection of TVO Kids Link in Footer */
			
			linkFoo[6] = RedirectLink("targetXpath", "TVOKids");
			assertEquals(linkFoo[6], properties.getProperty("footerLinkTarget"));
			
			linkFoo[7] = RedirectLink("hrefXpath", "TVOKids");
			assertEquals(linkFoo[7], properties.getProperty("tvoKidsHref"));

			/* Redirection of mPower Link in Footer */
			
			linkFoo[8] = RedirectLink("targetXpath", "mPower");
			assertEquals(linkFoo[8], properties.getProperty("footerLinkTarget"));
			
			linkFoo[9] = RedirectLink("hrefXpath", "mPower");
			assertEquals(linkFoo[9], properties.getProperty("mPowerHref"));
			
			
			/* Redirection of iLC Link in Footer */
			
			linkFoo[10] = RedirectLink("targetXpath", "ILC");
			assertEquals(linkFoo[10], properties.getProperty("footerLinkTarget"));
			
			linkFoo[11] = RedirectLink("hrefXpath", "ILC");
			assertEquals(linkFoo[11], properties.getProperty("iLCHref"));
		
			
			/* Redirection of GED Testing Link in Footer */
			
			linkFoo[12] = RedirectLink("targetXpath", "GED Testing");
			assertEquals(linkFoo[12], properties.getProperty("footerLinkTarget"));
			
			linkFoo[13] = RedirectLink("hrefXpath", "GED Testing");
			assertEquals(linkFoo[13], properties.getProperty("gedTestingHref"));
			
			/* Redirection of Facebook Link in Footer */
			WebElement fb = driver.findElement(By.xpath(properties.getProperty("fbLinkXpath")));
			linkFoo[14] =(String)((JavascriptExecutor)driver).executeScript(properties.getProperty("targetXpath"), fb);
			assertEquals(linkFoo[14], properties.getProperty("footerLinkTarget"));
			
			linkFoo[15] =(String)((JavascriptExecutor)driver).executeScript(properties.getProperty("hrefXpath"), fb);
			assertEquals(linkFoo[15], properties.getProperty("fbHref"));
			
			/* Redirection of Facebook Link in Footer */
			
			WebElement twitter = driver.findElement(By.xpath(properties.getProperty("twitterLinkXpath")));
			linkFoo[16] =(String)((JavascriptExecutor)driver).executeScript(properties.getProperty("targetXpath"), twitter);
			assertEquals(linkFoo[16], properties.getProperty("footerLinkTarget"));
			
			linkFoo[17] =(String)((JavascriptExecutor)driver).executeScript(properties.getProperty("hrefXpath"), twitter);
			assertEquals(linkFoo[17], properties.getProperty("twitterHref"));
			
			/* Redirection of Newsletter Subscribe Link in Footer */
			
			ElementClick("subscribeNewsLetterFooter");
			redirectURL = driver.getCurrentUrl();
			Assert.assertEquals(redirectURL, expectedURL[1]);
			System.out.println("Newsletter Subscription Page - Primary Landing Page Program");
			if (redirectURL.equals(expectedURL[1])) {
			
			
			
			driver.navigate().back();
			} 
			


}

@Test (priority=5)
public void aboutUs() throws InterruptedException, IOException {
			
			WebDriverWait wait3 = new WebDriverWait(driver, 2);
			// About Us page
			ElementClick("aboutUsQuickLink");
			GetElement("tvoLogo").isDisplayed();
			wait3.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(properties.getProperty("activeEnable"))));
			GetElement("educatorTopLink").isDisplayed();
			GetElement("parentsTopLink").isDisplayed();
			GetElement("studentTopLink").isDisplayed();
			assertEquals(getText("registerTopLink"), "Register");
			assertEquals(getText("loginTopLink"), "Login");
			
			assertEquals(getText("aboutUsText"), "About Us");
			assertTrue(VerifyImage("aboutUsImg"));
			
			str[60] = FindElements("aboutUsTextHeader1Xpath");
			CompareStrings(str[60], ReadProperty("aboutUsTextHeader1"));
			str[61] = FindElements("aboutUsPara1Xpath");
			CompareStrings(str[61], ReadProperty("aboutUsPara1"));
			str[62] = FindElements("aboutUsTextHeader2Xpath");
			CompareStrings(str[62], ReadProperty("aboutUsTextHeader2"));
			
			str[63] = FindElements("aboutUsPara2Xpath");
			CompareStrings(str[63], ReadProperty("aboutUsPara2"));
			
			str[64] = FindElements("aboutUsTextHeader3Xpath");
			CompareStrings(str[64], ReadProperty("aboutUsTextHeader3"));
			
			str[65] = FindElements("aboutUsPara3Xpath");
			CompareStrings(str[65], ReadProperty("aboutUsPara3"));
			
			str[66] = FindElements("aboutUsTextHeader4Xpath");
			CompareStrings(str[66], ReadProperty("aboutUsTextHeader4"));
			
			str[67] = FindElements("aboutUsPara4Xpath");
			CompareStrings(str[67], ReadProperty("aboutUsPara4"));
			
			str[68] = FindElements("learnMoreXpath");
			CompareStrings(str[68], ReadProperty("learnMore"));
			
			driver.findElement(By.linkText("here")).isDisplayed();
			ElementClick("tvoOrgLink");
			redirectURL = driver.getCurrentUrl();
           
            Assert.assertEquals(redirectURL, expectedURL[0]);
            System.out.println("About Us Link - Primary Landing Page Program");
            if (redirectURL.equals(expectedURL[0])) {
            System.out.println("Navigated to respective page");
            	}
          	driver.navigate().back();
          	
          	GetElement("footerMain").isDisplayed();
          	GetElement("footerSub").isDisplayed();
          	
}

@Test (priority=6)
public void contactUs() throws InterruptedException, IOException {
	
			
			// Contact Us page
			ElementClick("contactUsQuickLink");
			GetElement("tvoLogo").isDisplayed();
			GetElement("educatorTopLink").isDisplayed();
			GetElement("parentsTopLink").isDisplayed();
			GetElement("studentTopLink").isDisplayed();
			assertEquals(getText("registerTopLink"), "Register");
			assertEquals(getText("loginTopLink"), "Login");
			assertEquals(getText("contactUsText"), "Contact Us");
			assertTrue(VerifyImage("contactUsImg"));
			assertEquals(getText("contactUsQuesText1"), "Have a Question?");
			assertEquals(getText("contactUsQuesText2"), "Please note, it may take us up to 24 hours to respond.");
			GetElement("contactUsEmail").isDisplayed();
			assertEquals(getText("contactUsMailingAddr"), "Mailing Address:");
			assertEquals(getText("contactUsMailAddr1"), "Mathify");
			assertEquals(getText("contactUsMailAddr2"), "PO Box 200, Station Q");
			assertEquals(getText("contactUsMailAddr3"), "Toronto ON");
			assertEquals(getText("contactUsMailAddr4"), "M4T2T1");
			assertEquals(getText("contactUsNameText"), "Name");
			GetElement("contactUsNameField").isEnabled();
			GetElement("contactUsNameField").sendKeys("Test1");
			assertEquals(getText("contactUsEmailText"), "Email*");
			GetElement("contactUsEmailField").isEnabled();
			GetElement("contactUsEmailField").sendKeys("Test1@test.com");
			assertEquals(getText("contactUsReasonsText"), "Reasons for contacting us*");
			GetElement("contactUsReasonsField").sendKeys("Contact Us Test");
			GetElement("contactUsMessageArea").isEnabled();
			GetElement("contactUsMessageArea").sendKeys("Testing Contact Us page");
			assertEquals(getText("contactUsMessageText"), "Message"); //Not Submitting, due to captcha
			GetElement("footerMain").isDisplayed();
          	GetElement("footerSub").isDisplayed();
          	System.out.println("Success - Primary Landing Page program");
}

}
