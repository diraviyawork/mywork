package tvo699_RegistrationforEducators;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import tvo616_LandingPageScenarios.QA_BaseClass;

public class QA_EducatorRegistrationInvalidUser extends QA_BaseClass {

	String[] str = new String[100]; 

	
	@DataProvider(name="InvalidEduTestData")
	public Object[][] testData1() {
		return new Object[][] {
			{ "QA-@Tes1","QA'TEst'12","QA", "qatest1@mail-tester.com", "Test123!", "45","eduRegisterGenericErrMsg1", false} ,
			{ "", "", "", "", "", "", "eduRegisterGenericErrMsg2",false} ,
			{ "Diya","Diraviya","diya03", "diya1@1skooli.com", "Test123!", "13", "eduRegisterExistingAccErrMsg", true} ,
			{ "QA'Test","QA-TEST","", "", "Test123!","2", "eduRegisterGenericErrMsg3", false} ,
			{ "Qa$@TE", "123TEs", "QATEST88","qatest7@mail-tester.com", "", "", "eduRegisterGenericErrMsg4",false} 
		} ;
	}
    
	
	
	@Test(priority=1, dataProvider="InvalidEduTestData") /* TVO-700 Student Registration Invalid User */
	public void StudentRegistrationInvalidUserTest(String FName, String LName, String Username, String Email, String Password, String board, String errorProp, boolean existsInDB) throws InterruptedException, IOException {
	    driver.get(testenvironment.url()+"register/educator");
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(properties.getProperty("tvoLogo"))));
		GetElement("tvoLogo").isDisplayed();
		
		GetElement("eduFirstNameField").clear();
		GetElement("eduFirstNameField").sendKeys(FName);

		GetElement("eduLastNameField").clear();
		GetElement("eduLastNameField").sendKeys(LName);

		GetElement("eduUsernameField").clear();
		GetElement("eduUsernameField").sendKeys(Username);
		
		GetElement("eduEmailAddressField").clear();
		GetElement("eduEmailAddressField").sendKeys(Email);
		
		GetElement("eduPasswordField").clear();
		GetElement("eduPasswordField").sendKeys(Password);
		
		Select boardSelect = new Select(GetElement("eduSelectSchoolBoardField"));
		boardSelect.selectByValue(board);
		
		if(!GetElement("agreementCheckBox").isSelected()) {
			
			GetElement("agreementCheckBox").click();
			
		}
		
		str[3] = RedirectLink("targetXpath", "Terms of Service");
		assertEquals(str[3], properties.getProperty("footerLinkTarget"));
		
		str[4] = RedirectLink("hrefXpath", "Terms of Service");
		assertEquals(str[4], properties.getProperty("termsHref"));
		
		str[5] = RedirectLink("targetXpath", "Privacy Policy");
		assertEquals(str[5], properties.getProperty("footerLinkTarget"));
		
		str[6] = RedirectLink("hrefXpath", "Privacy Policy");
		assertEquals(str[6], properties.getProperty("privacyHref"));
		
		assertEquals(getText("eduSubscribeTextXpath"), "Subscribe to our newsletters");
		
		GetElement("submitButton").isDisplayed();
		GetElement("submitButton").isEnabled();
		
		GetElement("footerMain").isDisplayed();
      	GetElement("footerSub").isDisplayed();
      	GetElement("submitButton").click();
      	
     // Validating error messages.
		if (existsInDB == false) {
			
			WebElement ele5 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='c-status-update animated fadeIn failure']")));
			String textmsg = ele5.getText();
			
			Assert.assertEquals(textmsg.trim(), ReadProperty(errorProp));
			} 
		else if (existsInDB = true) {
				WebElement ele5 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='error']")));
				String textmsg = ele5.getText();
				
				Assert.assertEquals(textmsg.trim(), ReadProperty(errorProp));
				} 
		 System.out.println("Success - Educator Registration Invalid User program");
			}

	}
