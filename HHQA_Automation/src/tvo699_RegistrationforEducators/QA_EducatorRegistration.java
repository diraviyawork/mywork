package tvo699_RegistrationforEducators;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import tvo616_LandingPageScenarios.QA_BaseClass;

public class QA_EducatorRegistration extends QA_BaseClass {
	String[] str = new String[100]; 
	String redirectURL= null;
	
	@Test(priority=1) /* Educator Registration */
	public void EducatorRegistrationTest() throws InterruptedException, IOException {
		
		driver.get(testenvironment.url()); 
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		GetElement("tvoLogo").isDisplayed();
		
		GetElement("studentTopLink").isDisplayed();
		GetElement("educatorTopLink").isDisplayed();
		GetElement("parentsTopLink").isDisplayed();
		GetElement("registerTopLink").isEnabled();
		assertEquals(getText("registerTopLink"), "Register");
		GetElement("loginTopLink").isEnabled();
		assertEquals(getText("loginTopLink"), "Login");
		GetElement("activeEnable").isDisplayed();
		
		ElementClick("registerTopLink");
		GetElement("registerToolTip").isDisplayed();
		assertTrue(VerifyImage("studentMenuIcon"));
		assertEquals(getText("studentTextToolTip"), "Student");
		assertTrue(VerifyImage("educatorMenuIcon"));
		assertEquals(getText("educatorTextToolTip"), "Educator");
		
		ElementClick("educatorSelectRegistration");
		redirectURL = driver.getCurrentUrl();
		Assert.assertEquals(redirectURL, testenvironment.url()+"register/educator");
		
		
		GetElement("headerMain").isDisplayed();
		assertTrue(VerifyImage("educatorRegistrationImg"));
		GetElement("educatorRegisterWrapper").isDisplayed();
		assertEquals(getText("educatorRegistrationTextXpath"), "Educator Registration");
		str[0] = FindElements("registrationMandatoryCommentLineXpath");
		CompareStrings(str[0], ReadProperty("registrationMandatoryCommentLineText"));
		
		assertEquals(getText("eduFirstNameTextXpath"), "First Name");
		GetElement("eduFirstNameField").isDisplayed();
		GetElement("eduFirstNameField").isEnabled();
		
		assertEquals(getText("eduLastNameTextXpath"), "Last Name");
		GetElement("eduLastNameField").isDisplayed();
		GetElement("eduLastNameField").isEnabled();
		
		
		assertEquals(getText("eduUsernameTextXpath"), "Username");
		assertEquals(getText("eduUsernameMinCharacTextXpath"), "Minimum of 5 characters");
		GetElement("eduUsernameField").isDisplayed();
		GetElement("eduUsernameField").isEnabled();
		
		assertEquals(getText("eduEmailAddressTextXpath"), "Email Address");
		assertEquals(getText("eduEmailAddressRequiredTextXpath"), "Board email address required");
		GetElement("eduEmailAddressField").isDisplayed();
		GetElement("eduEmailAddressField").isEnabled();
		
		assertEquals(getText("eduPasswordTextXpath"), "Password");
		assertEquals(getText("eduPasswordMinCharTextXpath"), "Minimum of 5 characters");
		GetElement("eduPasswordField").isDisplayed();
		GetElement("eduPasswordField").isEnabled();
		GetElement("eduTogglePasswordIcon").isDisplayed();
		
		assertEquals(getText("eduSchoolBoardTextXpath"), "School Board/Organization");
		GetElement("eduSelectSchoolBoardField").isDisplayed();
		GetElement("eduSelectSchoolBoardField").isEnabled();
		assertEquals(getText("selectBoardOptionXpath"), "Select Board");	
		
		GetElement("agreementCheckBox").isDisplayed();
		GetElement("agreementCheckBox").isEnabled();
		assertEquals(getText("eduAgreementTextXpath"), "I agree to the Terms of Service and Privacy Policy");  
		GetElement("newsCheckBox").isDisplayed();
		GetElement("newsCheckBox").isEnabled();
		
		str[3] = RedirectLink("targetXpath", "Terms of Service");
		assertEquals(str[3], properties.getProperty("footerLinkTarget"));
		
		str[4] = RedirectLink("hrefXpath", "Terms of Service");
		assertEquals(str[4], properties.getProperty("termsHref"));
		
		str[5] = RedirectLink("targetXpath", "Privacy Policy");
		assertEquals(str[5], properties.getProperty("footerLinkTarget"));
		
		str[6] = RedirectLink("hrefXpath", "Privacy Policy");
		assertEquals(str[6], properties.getProperty("privacyHref"));
		
		assertEquals(getText("eduSubscribeTextXpath"), "Subscribe to our newsletters");
		
		GetElement("submitButton").isDisplayed();
		GetElement("submitButton").isEnabled();
	
		GetElement("footerMain").isDisplayed();
      	GetElement("footerSub").isDisplayed();
      	System.out.println("Success - Educator Registration program");
}
}
