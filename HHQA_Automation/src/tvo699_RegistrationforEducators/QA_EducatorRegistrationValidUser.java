package tvo699_RegistrationforEducators;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import tvo616_LandingPageScenarios.QA_BaseClass;

public class QA_EducatorRegistrationValidUser extends QA_BaseClass {
	String[] str = new String[100]; 
	
	@DataProvider(name="ValidEduTestData")
	public Object[][] testData1() {
		return new Object[][] {
			{ "QA-Tes-","QA'TEst'12","QATest12345", "qatest12345@mail-tester.com", "Test123!" , "45"} ,
			{ "QA-Test'","QA''Est","123QATest", "123qatest@brocku.ca", "Test123!", "1" } ,
			{ "qates","qates","QATest2", "qatest2@tvo.org", "Test123!", "38"} ,
			{ "QA'Test","QA-TEST","QATEST666", "QATEST666@cpco.on.ca", "Test123!","68"} 
		} ;
	}

	
	
	@Test(priority=1, dataProvider ="ValidEduTestData") /* Educator Registration */
	public void EducatorRegistrationValidTest(String FName, String LName, String Username, String Email, String Password, String board) throws InterruptedException, IOException {
	    driver.get(testenvironment.url()+"register/educator");
	    GetElement("tvoLogo").isDisplayed();
		
		GetElement("eduFirstNameField").clear();
		GetElement("eduFirstNameField").sendKeys(FName);

		GetElement("eduLastNameField").clear();
		GetElement("eduLastNameField").sendKeys(LName);

		GetElement("eduUsernameField").clear();
		GetElement("eduUsernameField").sendKeys(Username);
		
		GetElement("eduEmailAddressField").clear();
		GetElement("eduEmailAddressField").sendKeys(Email);
		
		GetElement("eduPasswordField").clear();
		GetElement("eduPasswordField").sendKeys(Password);
		
		Select boardSelect = new Select(GetElement("eduSelectSchoolBoardField"));
		boardSelect.selectByValue(board);
		
		
		GetElement("agreementCheckBox").isDisplayed();
		GetElement("agreementCheckBox").isEnabled();
		assertEquals(getText("eduAgreementTextXpath"), "I agree to the Terms of Service and Privacy Policy");  
		GetElement("newsCheckBox").isDisplayed();
		GetElement("newsCheckBox").isEnabled();
		
		if(GetElement("agreementCheckBox").isSelected()) {
			
			GetElement("agreementCheckBox").click();
			
		}
		
		str[3] = RedirectLink("targetXpath", "Terms of Service");
		assertEquals(str[3], properties.getProperty("footerLinkTarget"));
		
		str[4] = RedirectLink("hrefXpath", "Terms of Service");
		assertEquals(str[4], properties.getProperty("termsHref"));
		
		str[5] = RedirectLink("targetXpath", "Privacy Policy");
		assertEquals(str[5], properties.getProperty("footerLinkTarget"));
		
		str[6] = RedirectLink("hrefXpath", "Privacy Policy");
		assertEquals(str[6], properties.getProperty("privacyHref"));
		
		assertEquals(getText("eduSubscribeTextXpath"), "Subscribe to our newsletters");
		
		GetElement("submitButton").isDisplayed();
		GetElement("submitButton").isEnabled(); //Not Submitting now, will submit after implementation of user deletion functionality
	
		GetElement("footerMain").isDisplayed();
      	GetElement("footerSub").isDisplayed();
      	System.out.println("Success - Educator Registration Valid User program");
}
	
}
